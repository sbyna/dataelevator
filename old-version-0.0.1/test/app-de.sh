if [[ -f DataElevatorMetaTable.db ]]; then
    rm DataElevatorMetaTable.db
fi

for f in testf.step*.h5p; do
    ## Check if the glob gets expanded to existing files.
    ## If not, f here will be exactly the pattern above
    ## and the exists test will evaluate to false.
    if [[ -e "$f" ]]; then
	rm "$f"
    fi
done

for f in testf.step*.h5p.on.bb; do
    ## Check if the glob gets expanded to existing files.
    ## If not, f here will be exactly the pattern above
    ## and the exists test will evaluate to false.
    if [[ -e "$f" ]]; then
	rm "$f"
    fi
done

if [[ -x "$(command -v srun)" ]]; then
  RUN_COMMAND=srun   
else
    if [[ -x "$(command -v mpirun)" ]]; then
	RUN_COMMAND=mpirun   
    else
	echo "Neither mpirun and srun exists for test"
	exit -1
    fi
fi

DE_JOB_BIN=
if [[ -e "../build/bin/dejob" ]]; then
    DE_JOB_BIN="../build/bin/dejob"
else
    if [[ -x "$(command -v dejob)" ]]; then
	DE_JOB_BIN="$(command -v dejob)"
    else
	echo "Cann't find the $DE_JOB_BIN and you may append dejob to your PATH "
	exit -1
    fi
fi


$RUN_COMMAND -n 2 ../example/simjob -f testf -g /testg -d /testg/testd -s 100 -n 10 
$RUN_COMMAND -n 2 $DE_JOB_BIN  -m

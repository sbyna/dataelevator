files_on_bb=("testf.step0.h5p.on.bb" "testf.step1.h5p.on.bb" "testf.step2.h5p.on.bb" "testf.step3.h5p.on.bb" "testf.step4.h5p.on.bb" "testf.step5.h5p.on.bb" "testf.step6.h5p.on.bb" "testf.step7.h5p.on.bb" "testf.step8.h5p.on.bb" "testf.step9.h5p.on.bb")


files_on_disk=("testf.step0.h5p" "testf.step1.h5p" "testf.step2.h5p" "testf.step3.h5p" "testf.step4.h5p" "testf.step5.h5p" "testf.step6.h5p" "testf.step7.h5p" "testf.step8.h5p" "testf.step9.h5p")

meta_file=DataElevatorMetaTable.db
right_file=testf.right.h5p


if [[ -e "$meta_file" ]]; then
    echo "$meta_file exists"
else
    echo "$meta_file is missing"
    exit -1
fi


for file in "${files_on_bb[@]}"
do
    if [[ -e "$file" ]]; then
	h5diff $right_file  $file
	retVal=$?
	if [[ $retVal -ne 0 ]]; then
	   exit -1
	fi
	echo "Check the existence and content of $file redirected to Burst Buffer by Data Elevator"
    else
	echo "$file is missing"
	exit -1
    fi
done

for file in "${files_on_disk[@]}"
do
    if [[ -e "$file" ]]; then
	h5diff  $right_file  $file
	retVal=$?
	if [[ $retVal -ne 0 ]]; then
	   exit -1
	fi
	echo "Check the existence and content of $file moved by Data Elevator from Burst Buffer to Disk"
    else
	echo "$file is missing"
	exit -1
    fi
done

exit 0

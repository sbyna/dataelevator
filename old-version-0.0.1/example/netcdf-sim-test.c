/*
 * Authors: Wu, Danqing       and     Krishna, Jayesh
 *          wuda@mcs.anl.gov  and    jayesh@mcs.anl.gov
 */

#include "hdf5.h"
#include <stdlib.h>

#define H5FILE_NAME "test_write_read.h5"
#define DATASETNAME "IntArray" 
#define NX 64
#define NY 5 
#define RANK 2
#define MAGIC_NUMBER_LEN 4

int main(int argc, char **argv)
{
  /*
   * HDF5 APIs definitions
   */
  hid_t file_id, dset_id; /* file and dataset identifiers */
  hid_t filespace, memspace; /* file and memory dataspace identifiers */
  hsize_t dimsf[2]; /* dataset dimensions */
  int *write_data; /* pointer to data buffer to write */
  int *read_data; /* pointer to data buffer to read */
  hsize_t	count[2]; /* hyperslab selection parameters */
  hssize_t offset[2];
  hid_t	plist_id; /* property list identifier */
  int i;
  herr_t status;

  /*
   * MPI variables
   */
  int mpi_size, mpi_rank;
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Info info = MPI_INFO_NULL;

  /*
   * Initialize MPI
   */
  MPI_Init(&argc, &argv);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);  

  /* 
   * Set up file access property list with parallel I/O access
   */
   plist_id = H5Pcreate(H5P_FILE_ACCESS);
   H5Pset_fapl_mpio(plist_id, comm, info);

  /*
   * Create a new file collectively and release property list identifier
   */
  file_id = H5Fcreate(H5FILE_NAME, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  H5Pclose(plist_id);   

  /*
   * Create the dataspace for the dataset
   */
  dimsf[0] = NX;
  dimsf[1] = NY;
  filespace = H5Screate_simple(RANK, dimsf, NULL); 

  /*
   * Create the dataset with default properties and close filespace.
   */
  dset_id = H5Dcreate(file_id, DATASETNAME, H5T_NATIVE_INT, filespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Sclose(filespace);

  /* 
   * Each process defines dataset in memory and writes it to the hyperslab
   * in the file.
   */
  count[0] = dimsf[0] / mpi_size;
  count[1] = dimsf[1];
  offset[0] = mpi_rank * count[0];
  offset[1] = 0;
  memspace = H5Screate_simple(RANK, count, NULL);

  /*
   * Select hyperslab in the file
   */
  filespace = H5Dget_space(dset_id);
  H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL, count, NULL);

  /*
   * Initialize write data buffer 
   */
  write_data = (int*)malloc(sizeof(int) * count[0] * count[1]);
  for (i = 0; i < count[0] * count[1]; i++)
    write_data[i] = mpi_rank + 10;

  /*
   * Create property list for collective dataset write
   */
  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
    
  H5Dwrite(dset_id, H5T_NATIVE_INT, memspace, filespace, plist_id, write_data);
  free(write_data);

  /*
   * Close/release resources.
   */
  H5Dclose(dset_id);
  H5Sclose(filespace);
  H5Sclose(memspace);
  H5Pclose(plist_id);
  H5Fclose(file_id);

  /* ============================== NetCDF simulation code starts ===================================================================================== */
  /* NetCDF routine NC_check_file_type (defined in libdispatch/dfile.c, called by NC_open) needs to read the file header */
  MPI_File fh;
  char err_msg[MPI_MAX_ERROR_STRING];
  int result_len;
  int retval = MPI_File_open(comm, H5FILE_NAME, MPI_MODE_RDONLY, info, &fh);
  /* MPI_File_open is likely to fail, since it takes some time for DE to move test_write_read.h5 from BB to disk in the background */
  if (retval != MPI_SUCCESS) {
    MPI_Error_string(retval, err_msg, &result_len);
    /* Here we just print out the error message if MPI_File_open() fails. Note, NetCDF will return status NC_EPARINIT in this case */
    if (mpi_rank == 0)
      fprintf(stderr, "Error message returned by MPI_File_open() is %s\n", err_msg);
  }

  /* NetCDF calls MPI_File_read() to read the magic number */
  /* Get the 4-byte magic from the beginning of the file */
  char magic[MAGIC_NUMBER_LEN];
  MPI_Status mstatus;
  retval = MPI_File_read(fh, magic, MAGIC_NUMBER_LEN, MPI_CHAR, &mstatus);
  if (retval != MPI_SUCCESS) {
    MPI_Error_string(retval, err_msg, &result_len);
    /* Here we just print out the error message if MPI_File_read() fails. Note, NetCDF will return status NC_EPARINIT in this case */
    if (mpi_rank == 0)
      fprintf(stderr, "Error message returned by MPI_File_read() is %s\n", err_msg);
  }
  else {
    if (mpi_rank == 0)
      printf("magic[1] = %c, magic[2] = %c, magic[3] == %c\n", magic[1], magic[2], magic[3]); /* "HDF" is expected */
  }

  MPI_File_close(&fh);

  /* call NC_interpret_magic_number() to figure out the file format and return that format value (NC_FORMATX_XXX) */
  /* ============================== NetCDF simulation code ends ======================================================================================= */

  /* 
   * Set up file access property list with parallel I/O access
   */
  plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, comm, info);

  /*
   * Open the file collectively
   */
  file_id = H5Fopen(H5FILE_NAME, H5F_ACC_RDONLY, plist_id);
  H5Pclose(plist_id);   

  /*
   * Open the dataset1 collectively
   */
  dset_id = H5Dopen(file_id, DATASETNAME, H5P_DEFAULT);

  /* 
   * Each process defines dataset in memory and writes it to the hyperslab
   * in the file.
   */
  count[0] = dimsf[0] / mpi_size;
  count[1] = dimsf[1];
  offset[0] = mpi_rank * count[0];
  offset[1] = 0;
  memspace = H5Screate_simple(RANK, count, NULL);

  /*
   * Select hyperslab in the file
   */
  filespace = H5Dget_space(dset_id);
  H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL, count, NULL);

  /*
   * Initialize read data buffer 
   */
  read_data = (int*)malloc(sizeof(int) * count[0] * count[1]);

  /*
   * Create property list for collective dataset read
   */
  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

  status = H5Dread(dset_id, H5T_NATIVE_INT, memspace, filespace, plist_id, read_data);
  /*
  if (mpi_rank == 0) {
    for (i = 0; i < count[0] * count[1]; i++)
      printf("%d ", read_data[i]);
    printf("\n");
  }
  */
  free(read_data);

  /*
   * Close/release resources.
   */
  H5Dclose(dset_id);
  H5Sclose(filespace);
  H5Sclose(memspace);
  H5Pclose(plist_id);
  H5Fclose(file_id);

  MPI_Finalize();

  return 0;
}

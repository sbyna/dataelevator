#include "stdlib.h"
#include "hdf5.h"
#include "getopt.h"
#include <string.h>
#include "mpi.h"

void print_help(){
   char *msg="Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -f header of file (add_step[stepnumber].h5p by default) \n\
          -g group path within HDF5 file to data set \n\
          -d name of the dataset \n\
          -s size for each core (by default 1D int data) \n\
	  -n the number steps \n\
	  -t interval (seconds) between steps \n\
          Example:  mpirun -n 1 ./simjob -f testf.h5p -g /testg -d /testg/testd -s 100 -n 3 \n";

   fprintf(stdout, msg, "test-simulation");
}


int main(int argc, char **argv){
  int      mpi_size, mpi_rank, dims, i, c, n=1, t=5, fake_error_flag=0;
  hid_t    file_id, dset_id, group_id, plist_id, dataspace_id, write_space, memspace_id;
  hsize_t  offset, count, dims_size[1];
	
  char     filename[NAME_MAX], filename_head[NAME_MAX],  groupname[NAME_MAX], dsetname[NAME_MAX];
  
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Info info = MPI_INFO_NULL;
  
     
  while ((c = getopt (argc, argv, "f:g:d:s:n:t:he")) != -1)
    switch (c)
    {
      case 'f':
        strncpy(filename_head, optarg, NAME_MAX);
        break;
      case 'g':
        strncpy(groupname, optarg, NAME_MAX);
        break;
      case 'd':
        strncpy(dsetname, optarg, NAME_MAX);
        break;
      case 's':
	count = atoi(optarg);
	break;
      case 'n':
	n = atoi(optarg);
	break;
      case 't':
	t = atoi(optarg);
	break;
      case 'e':
        fake_error_flag = 1;
        break;
      case 'h':
        print_help();
        return 1;
        break;
      default:
        break;
    }
  
  MPI_Init(&argc, &argv);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  
  int j; 
  for(j = 0; j < n; j++){	  
        plist_id = H5Pcreate(H5P_FILE_ACCESS);
  	H5Pset_fapl_mpio(plist_id, comm, info);
    
	sprintf(filename, "%s.step%d.h5p", filename_head, j);
	if(mpi_rank == 0)
          printf("[Sim job] Using %d cores to write file [%s]. \n", mpi_size, filename);
 
  	file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  	H5Pclose(plist_id);


  	group_id  = H5Gcreate(file_id, groupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        dims = 1;
        dims_size[0] = count * mpi_size; 		
        dataspace_id = H5Screate_simple(dims, dims_size, NULL);
        dset_id    = H5Dcreate(group_id, dsetname, H5T_STD_I32LE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Sclose(dataspace_id);
 
        offset  = count * mpi_rank;
  
        write_space = H5Dget_space(dset_id);
        H5Sselect_hyperslab(write_space, H5S_SELECT_SET, &offset, NULL, &count, NULL);

        memspace_id = H5Screate_simple(dims, &count, NULL);
  
        hsize_t data_size = 1;
 
        float size_in_gb =  count*sizeof(int)/1024.0/1024.0/1024.0;
        int            *pi;
        pi  = malloc(count * sizeof(int));
        if(pi == NULL){
          printf("Memory allocation fails (size=%f) \n", size_in_gb);
          exit(1);
        }

        for(i = 0; i < count; i++){
          pi[i] = i;
        }
 
        if(mpi_rank == 0){
          printf("[Sim job] Write data to %s ...  ", filename);
        }


        H5Dwrite(dset_id, H5T_NATIVE_INT,  memspace_id, write_space, H5P_DEFAULT, pi);
        if(mpi_rank == 0){
          printf(" Done !!! \n");
        }


        free(pi);

        H5Sclose(write_space);
        H5Sclose(memspace_id);
  	H5Dclose(dset_id);
  	H5Gclose(group_id);
  	H5Fclose(file_id);

        if(mpi_rank == 0){
          printf(" [Sim job]\n ... Computing (time )... \n \n");
        }

        fflush(stdout);

	sleep(t);
        if(fake_error_flag && (j == n/2)){
          exit(-1);
        }
  }
  MPI_Finalize();
}

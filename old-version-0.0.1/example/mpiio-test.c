#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <mpi.h>

#ifndef NAME_MAX
#define NAME_MAX 512
#endif

void print_help(){
   char *msg="Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -f header of file (add_step[stepnumber].mpifile by default) \n        \
          -s number of float points (x1M) to store per mpi-rank \n             \
	  -n the number steps \n                           \
	  -t interval (seconds) between steps \n\
          Example:  mpirun -n 1 ./mpiio-test -f testf -s 10 -n 3 \n";

   fprintf(stdout, msg, "mpiio-test");
}


int main(int argc, char **argv){
  int      mpi_size, mpi_rank, i, c, n=1, sleep_time=2, fake_error_flag=0;
  MPI_File fh;
  int      count = 1024*1024; //1G
  char     filename[NAME_MAX], filename_head[NAME_MAX];
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Info info = MPI_INFO_NULL;
  MPI_Status status;

     
  while ((c = getopt (argc, argv, "f:s:n:t:he")) != -1)
    switch (c)
    {
      case 'f':
        strncpy(filename_head, optarg, NAME_MAX);
        break;
      case 's':
	count = atoi(optarg) * count;
	break;
      case 'n':
	n = atoi(optarg) ;
	break;
      case 't':
	sleep_time = atoi(optarg);
	break;
      case 'e':
        fake_error_flag = 1;
        break;
      case 'h':
        print_help();
        return 1;
        break;
      default:
        break;
    }
  
  MPI_Init(&argc, &argv);
  MPI_Comm_size(comm, &mpi_size);
  MPI_Comm_rank(comm, &mpi_rank);
  
  int j; 
  for(j = 0; j < n; j++){	  
    sprintf(filename, "%s.step%d.mpifile", filename_head, j);
	if(mpi_rank == 0)
          printf("[Sim job] Using %d cores to write file [%s]. \n", mpi_size, filename);
        float size_in_gb =  count*sizeof(int)/1024.0/1024.0/1024.0;
        float           *pi_buf;   pi_buf  = malloc(count * sizeof(float));
        if(pi_buf == NULL){
          printf("Memory allocation fails (size=%f) \n", size_in_gb);
          exit(1);
        }
        
        for(i = 0; i < count; i++){
          pi_buf[i] = i;
        }
 
        if(mpi_rank == 0){
          printf("[Sim job] Write data to %s ... \n ", filename);
        }
        MPI_File_open(comm, filename, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);
        if(mpi_rank == 0)
          printf("[Sim job] FD value = [%lld]. \n", fh);

        MPI_File_write_ordered( fh, pi_buf, count, MPI_FLOAT, &status );
        
        if(mpi_rank == 0){
          printf(" [Sim job] Writing is finished !!! \n");
        }


        free(pi_buf);

        MPI_File_close(&fh);
        
        if(mpi_rank == 0){
          printf(" [Sim job]\n ... Computing (time )... \n \n");
        }
        
	sleep(sleep_time);
        if(fake_error_flag && (j == n/2)){
          exit(-1);
        }
  }
  MPI_Finalize();
}

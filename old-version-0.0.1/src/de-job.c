#include <stdio.h>   /* all IO stuff lives here */
#include <stdlib.h>  /* exit lives here */
#include <unistd.h>  /* getopt lives here */
#include <string.h>  /* strcpy lives here */
#include <limits.h>  /* INT_MAX lives here */
#include <mpi.h>     /* MPI and MPI-IO live here */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <signal.h>

int app_failed_flag = 0;

void my_handler(int signum)
{
  printf("Data Elevator: Received SIGUSR1 from application to terminate ! \n");
  if (signum == SIGUSR1)
  {
    app_failed_flag = 1;
  }
}

#define DEBUG 1
#include "de-job.h"
#include "record-op-file.h"
#include "op-journal.h"


#ifdef  DW_API
#include <datawarp.h>
#endif

#define MASTER_RANK 0
#define TRUE 1
#define FALSE 0
#define BOOLEAN int
#define MBYTE 1048576.0
#define GBYTE 1024*1024*1024
#define SYNOPSIS printf ("synopsis: %s -f <file>\n", argv[0])


void  printf_help(char *cmd){
     char *msg="Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -a overlap reading and writing \n\
          -c x: chunk size = x * 8MB \n\
          -d use DataWarp API (disabled by default) \n\
          -i independent MPI-IO  \n\
          -s set striping size to be chunk size  \n\
          -m keep temp files on burst buffer  \n\
          -r log file        \n\
          Example: srun -n 128 %s -a -i -s -r result.log \n";

     fprintf(stdout, msg, cmd, cmd);
}



int main(int argc, char *argv[])
{
  /* my variables */
  int my_rank, pool_size, last_guy, i, count, filename_length;
  int filename_output_length, *junk, file_open_error;
  BOOLEAN i_am_the_master = FALSE;

  char filename[NAME_LENGTH], filename_output[NAME_LENGTH],  *read_buffer;
  char *rfilename;
  /* MPI_Offset is long long */
  MPI_Offset number_of_bytes, my_offset, my_current_offset, total_number_of_bytes, number_of_bytes_ll, max_number_of_bytes_ll;
  double     data_size_all=0;
  MPI_File   fh, fho;
  MPI_Status status;
  double start, finish, read_time = 0 , write_time = 0, longest_io_time, read_time_all = 0, write_time_all=0, min_read_time = 0, min_write_time = 0, metadata_time = 0, rank0_write_time = 0, rank0_write_time_start, rank0_write_time_end, restart_time = 0, min_restart_time = 0, max_restart_time = 0;

  //Chunk size
  size_t chunk_size = BASIC_REQUEST_SIZE, chunk_temp;     //8MB, basic chunk size
  //Result file pointer 
  FILE *rfp;
  //Different flags variable
  int collective_io = 0;
  int async_io = 0;
  int set_striping_size_flag = 0;
  int result_file_flag = 0;
  int remove_file_flag = 1;
  int ops_journal_flag = 0;   //record the ops. 
  int fake_error_flag  = 0;   //Fake error flag

  int use_datawarp_stage_out = 0, build_index = 0, query_index = 0;

  signal(SIGUSR1, my_handler);

  /* ACTION */
  extern char *optarg; 
  int c;
  while ((c = getopt(argc, argv, "c:r:ijfdahsm")) != EOF)
    switch(c) {
      case 'd':
        use_datawarp_stage_out = 1;
        break;
      case 'r':
        result_file_flag = 1;
        rfilename =  strdup(optarg);
        //if(my_rank == 0)  printf("Log file %s \n", rfilename);
        break;
      case 'm':
        remove_file_flag = 0;
        break;
      case 'i':
	collective_io = 0;	
	break;
      case 'a':
        async_io = 1;
        break;
      case 'c': 
        chunk_temp = strtoul(optarg, NULL, 0); //chunk_temp < 4096
        chunk_size = chunk_size * chunk_temp;  //MB
        break;
      case 'j':
        ops_journal_flag = 1;
        break;
      case 'h':
        printf_help(argv[0]);
        return 0;
        break;
      case 's':
        set_striping_size_flag = 0;
        break;
      case 'f':
        fake_error_flag = 1;
        break;
      default:
        printf("Wrong option [%c] for %s \n",c, argv[0]);
        printf_help(argv[0]);
        MPI_Abort(MPI_COMM_WORLD, 1);
        break;
    } /* end of switch(c) */

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &pool_size);
  last_guy = pool_size - 1;
  if (my_rank == MASTER_RANK) i_am_the_master = TRUE;
  
  
  //Update the name (path) of the file storing metadata table
  if(my_rank == 0){
    NameDB();  
    if(result_file_flag == 1){
      rfp = fopen(rfilename, "w");
      if(rfp == NULL){
        printf("Error in open log file: %s \n", rfilename);
        exit(-1);
      }else{
        printf("Log file is: %s \n", rfilename);
      }
    }
    if(ops_journal_flag == 0){
      printf("TIMB2D starts to work ! \n");
      if (result_file_flag == 1)
        fprintf(rfp, "TIMB2D starts to work ! \n");
    }else{
      printf("TIMB2D HL starts to work ! \n");
      if (result_file_flag == 1)
        fprintf(rfp, "TIMB2D HL starts to work ! \n");
    }
  }
  
  fflush(stdout);

  
  int rf, ite = 0, first_write, first_stage_out = 1;

  //return 0;
  
  //Start to move data from BB to Disk
  while (1){
    //Wait the next available file
    if(my_rank == 0){
      rf = next_file(filename, filename_output);
      if(rf == -1)
        exit(-1);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(filename,        NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
    MPI_Bcast(filename_output, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
    
    //Finish signal of the simulaiton job
    if(strcmp(filename, "NULL")==0 && strcmp(filename_output, "NULL") == 0){
      break;
    }

    if ( (fake_error_flag == 1) && (ite==2) ){
      exit(-1);
    }
    
    //Check whether we has a log file

    start = MPI_Wtime();
    int last_moved_chunk_batch = 0;
    if(ops_journal_flag == 1 ){
      if(check_log_file(filename) == 1){
        //Some failure happes last time 
        //We continues the operations from last_moved_chunk_batch
        //All process checks and get the same last_moved_chunk_batch
        last_moved_chunk_batch = read_log_file(filename);
      }else{
        //DataElevator is trasfering a new file
        //Ranks zero creates log file for operations
        if(my_rank == 0)
          create_log_file(filename);
      }
    }
    finish = MPI_Wtime();
    restart_time = restart_time + finish - start;
    
    //Be default, we suggest users to use Data Elevator 
    if(use_datawarp_stage_out == 1){
#ifdef  DW_API
      rank0_write_time_start = MPI_Wtime();
      int ret = dw_stage_file_out(filename, filename_output, DW_STAGE_IMMEDIATE);
      if (ret < 0) {
        printf("dw_stage_file_out failed \n");
        exit(-1);
      }

      ret = dw_wait_file_stage(filename);
      if (ret < 0) {
        printf("dw_wait_file_stage failed \n");
        exit(-1);
      }
      
      rank0_write_time_end = MPI_Wtime();
      rank0_write_time = rank0_write_time + (rank0_write_time_end -  rank0_write_time_start);
      goto  stage_out;
#else
      printf("No support to for DataWarp api ! \n ");
#endif
    }
    first_write = 1;
    

    //Open the file on BB
    file_open_error = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
    if (file_open_error != MPI_SUCCESS) {
      char error_string[BUFSIZ];
      int length_of_error_string, error_class;
      MPI_Error_class(file_open_error, &error_class);
      MPI_Error_string(error_class, error_string, &length_of_error_string);
      printf("%3d: %s\n", my_rank, error_string);
      MPI_Error_string(file_open_error, error_string, &length_of_error_string);
      printf("%3d: %s\n", my_rank, error_string);
      MPI_Abort(MPI_COMM_WORLD, file_open_error);
    }
    
    //Get the file size
    MPI_File_get_size(fh, &total_number_of_bytes);
#ifdef DEBUG
    printf("%3d: total_number_of_bytes = %lld\n", my_rank, total_number_of_bytes);
#endif
    double temp_size  = temp_size + ((double)total_number_of_bytes)/MBYTE;
    temp_size         = temp_size/1024.0;
    data_size_all = data_size_all + temp_size; //To GB
    
    //update the striping size
    //number_of_bytes_ll = total_number_of_bytes / pool_size;
    //the stripe_size value must be a multiple of 64 KB (65536) 	 
    MPI_Offset chunks_by64k, chunks_by64k_pp, basic_strip_size= 65536; //
    if(chunk_size > total_number_of_bytes/pool_size){
      //Choose a multiple of 64KB as chunk size (=striping size)
      if(total_number_of_bytes % basic_strip_size == 0){	
      	chunks_by64k = total_number_of_bytes/basic_strip_size;
      }else{
        chunks_by64k = total_number_of_bytes/basic_strip_size + 1;
      }
      
      if (chunks_by64k % pool_size == 0){
	chunks_by64k_pp = chunks_by64k/pool_size;
      }else{
	chunks_by64k_pp = chunks_by64k/pool_size + 1;
      }
      chunk_size = chunks_by64k_pp * basic_strip_size;
    }
    
    //More options here for optimizations  
    //http://www2.cs.uh.edu/~gabriel/courses/cosc6374_s08/ParCo_18_ParallelIO3.pdf
    //http://mpi.deino.net/mpi_functions/MPI_File_write_ordered.html
    if((my_rank==0) && (set_striping_size_flag ==1)){
      //Set the striping size = chunk_size and also create a new file
      set_striping_size(filename_output, chunk_size); 
#ifdef DEBUG
      if(ite%10 == 0) printf("Set striping size to %llu, for file [%s] \n", chunk_size, filename_output);
#endif
    }
    
    //Wait here for the finish of set_striping_size 
    MPI_Barrier(MPI_COMM_WORLD);
    
    //Set some MPI-IO hints 
    MPI_Info infoo;
    char stripe_factor[256], stripe_unit[256]; //cb_buffer_size[256] = "400000000";
    MPI_Info_create(&infoo);
    MPI_Info_set(infoo, "access_style", "write_once, sequential");
    sprintf(stripe_factor, "%d ", OST_NUMS);
    sprintf(stripe_unit,   "%u ", chunk_size);
    MPI_Info_set(infoo, "striping_factor", stripe_factor);
    MPI_Info_set(infoo, "striping_unit",   stripe_unit);
    MPI_Info_set(infoo, "cb_nodes",        stripe_factor);
    
    //Open the file on Disk
    file_open_error = MPI_File_open(MPI_COMM_WORLD, filename_output,   MPI_MODE_CREATE|MPI_MODE_WRONLY, infoo, &fho);
    //MPI_File_set_atomicity(fho, 0);
    if (file_open_error != MPI_SUCCESS) {
      char error_string[BUFSIZ];
      int length_of_error_string, error_class;
      MPI_Error_class(file_open_error, &error_class);
      MPI_Error_string(error_class, error_string, &length_of_error_string);
      printf("%3d: %s\n", my_rank, error_string);
      MPI_Error_string(file_open_error, error_string, &length_of_error_string);
      printf("%3d: %s\n", my_rank, error_string);
      MPI_Abort(MPI_COMM_WORLD, file_open_error);
    }
    //MPI_File_set_size(fho, total_number_of_bytes);
    //MPI_File_preallocate(fho, total_number_of_bytes);
    
#ifdef DEBUG
    if(my_rank == 0){
      printf("Start to write file[%s] from BB to disk, chunk size = %u  \n ", filename_output, chunk_size);
    }
#endif
    
    if (result_file_flag == 1 && my_rank ==0)
      fprintf(rfp,"Start to write file[%s] from BB to disk, chunk size = %u  \n ", filename_output, chunk_size);
    
    //Start to read and write data
    MPI_Offset  iteration, total_chunks;
    fflush(stdout);
    if (total_number_of_bytes % chunk_size == 0){
      total_chunks = total_number_of_bytes / chunk_size;
    }else{
      total_chunks = total_number_of_bytes / chunk_size + 1;
    }
    if( total_chunks % pool_size == 0){
      iteration = total_chunks / pool_size;
    }else{
      iteration = total_chunks / pool_size + 1;
    }
    
    read_buffer = (char*) malloc(chunk_size);
    if(read_buffer == NULL){
      printf("Chunk size is too large to overflow the memory  !\n ");
      exit(-1);
    }
    
#ifdef DEBUG
    if((my_rank == 0) && (ite%10 == 0))
      printf("Total chunks = %llu, iterations = %llu, chunk_size = %u \n ", total_chunks, iteration, chunk_size);
#endif

    MPI_Offset ii = 0, chunk_offset, my_current_id = my_rank;
    int io_size, previous_write_io_size,count, ret, last_write_req_issued=0;
    MPI_Request write_req, read_req;

    //Restart from the last chunks, which failed for some reason
    if(ops_journal_flag ==  1){
      if (last_moved_chunk_batch > 0) {
        ii = last_moved_chunk_batch;
        my_current_id = pool_size * ii +  my_rank;
      }else{
        ii = 0;
      }
    }
    
    for(; ii < iteration; ii++){
      if(my_current_id >= total_chunks){
	if(collective_io == 0) break;	     
        io_size = 0; //or break
        chunk_offset = total_number_of_bytes; //To the end 
      }else{
        chunk_offset = my_current_id * chunk_size;
        if(chunk_offset > total_number_of_bytes){
         //io_size =  0; //or break
	 if(collective_io == 0) break;
        }

        if(chunk_offset + chunk_size  > total_number_of_bytes){
          io_size  =  total_number_of_bytes - chunk_offset;
        }else{
          io_size  =  chunk_size;
        }
      }
      
      
#ifdef DEBUG
      if(my_rank == 0 || my_rank == (pool_size -1) || my_rank == (pool_size -2))
        printf("rank = %d, chunk_offset = %llu, ii = %llu, io_size = %llu \n ", my_rank, chunk_offset, ii, io_size);
#endif


      //Read data
      start = MPI_Wtime();
      if(async_io == 1){
        ret = MPI_File_iread_at(fh, chunk_offset, read_buffer, io_size, MPI_BYTE, &read_req);
      }else{
        //MPI_File_seek(fh,  chunk_offset, MPI_SEEK_SET);
        ret = MPI_File_read_at(fh, chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
      }
      if(ret != MPI_SUCCESS){
        printf("Something is worong in reading ! \n");
        exit(-1);
      } 
      if(async_io == 1){
        MPI_Wait(&read_req, &status);
      }
      
      
      MPI_Get_count(&status, MPI_BYTE, &count);
      if(count != io_size) {
        printf("In reading, size mismatch : count = %d, io_size = %d ! \n", count, io_size);
        exit(-1);
      }
      
      finish = MPI_Wtime();
      read_time = read_time + (finish - start);

      //MPI_Barrier(MPI_COMM_WORLD);

      //Write data
      start = MPI_Wtime();
      rank0_write_time_start = MPI_Wtime();
      if(collective_io == 0){
        if(async_io == 1){
          if(first_write == 1){
            first_write = 0;
          }else{
            MPI_Wait(&write_req, &status);
            MPI_Get_count(&status, MPI_BYTE, &count);
            if (count != previous_write_io_size) {
              printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, previous_write_io_size);
              //exit(-1);
            }
          }
          ret = MPI_File_iwrite_at(fho, chunk_offset,  read_buffer, io_size, MPI_BYTE, &write_req);
          previous_write_io_size = io_size;
          last_write_req_issued = 1;
        }else{
          //MPI_File_seek(fho, chunk_offset, MPI_SEEK_SET);
          ret = MPI_File_write_at(fho,chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
        }
      }else{
      	//ret = MPI_File_write_ordered(fho, read_buffer, io_size, MPI_BYTE, &status);
        ret = MPI_File_write_at_all(fho, chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
      }
      if(ret != MPI_SUCCESS){
        printf("Something is worong in wrting ! \n");
        exit(-1);
      } 

      if(async_io == 0){
        MPI_Get_count(&status, MPI_BYTE, &count);
        if (count != io_size) {
          printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, io_size);
          exit(-1);
        }
      }
      
      finish = MPI_Wtime();
      rank0_write_time_end = MPI_Wtime();
      write_time = write_time + (finish - start);
      rank0_write_time =  rank0_write_time + (rank0_write_time_end - rank0_write_time_start);
      my_current_id = my_current_id + pool_size;


      if(ops_journal_flag == 1){
        MPI_Barrier(MPI_COMM_WORLD);
        if(my_rank == 0) //move log forward
          update_log_file(filename);
      }
    } //End of sinlge file
    
    //wait until the last write to finish
    if((async_io == 1) && (last_write_req_issued == 1)){
      start = MPI_Wtime();
      MPI_Wait(&write_req, &status);
      MPI_Get_count(&status, MPI_BYTE, &count);
      if (count != previous_write_io_size) {
        printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, previous_write_io_size);
      }
      finish = MPI_Wtime();
      rank0_write_time_end = MPI_Wtime();
      write_time = write_time + (finish - start);
      rank0_write_time =  rank0_write_time + (rank0_write_time_end - rank0_write_time_start);
    }    
    if(my_rank == 0 && ite== 0)
      printf("Writing is done ! \n ");

    if (my_rank == 0 && last_moved_chunk_batch > 0 && ops_journal_flag == 1) {
      MPI_Allreduce(&restart_time, &max_restart_time, 1, MPI_DOUBLE,   MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&restart_time, &min_restart_time, 1, MPI_DOUBLE,   MPI_MIN, MPI_COMM_WORLD);
      printf("Restart time (max)   = %f seconds, min = %f \n", max_restart_time, min_restart_time);
    }
    
    fflush(stdout);

    
    MPI_File_close(&fh);
    MPI_File_close(&fho);

    MPI_Barrier(MPI_COMM_WORLD);
    if(ops_journal_flag == 1 && my_rank == 0){
      delete_log_file(filename);
    }

    free(read_buffer);
 stage_out:
    start = MPI_Wtime();
    if(my_rank == 0){
      //printf("I am here ! Test \n \n");

      char  sv[256];
      sprintf(sv, "%d", DE_WRITE_DISK_DONE);
      UpdateRecordVOL(filename, DE_TYPE_S, sv);
      if(remove_file_flag == 1)
        MPI_File_delete(filename, MPI_INFO_NULL);
#ifdef DEBUG
      if(ite % 2 == 0)
      printf("  Done and Deleted, Waiting for next file .... \n");
#endif
    }
    finish = MPI_Wtime();
    metadata_time = metadata_time + (finish - start);

    ite++;
    if(ite > MAX_ITERATION) break;
  

  }

  
  
  MPI_Allreduce(&read_time, &longest_io_time, 1, MPI_DOUBLE,   MPI_MAX, MPI_COMM_WORLD);
  read_time_all  =  longest_io_time;
  
  MPI_Allreduce(&read_time, &longest_io_time, 1, MPI_DOUBLE,   MPI_MIN, MPI_COMM_WORLD);
  min_read_time  =  longest_io_time;
  
  MPI_Allreduce(&write_time, &longest_io_time, 1, MPI_DOUBLE,  MPI_MAX, MPI_COMM_WORLD);
  write_time_all = longest_io_time;
  
  MPI_Allreduce(&write_time, &longest_io_time, 1, MPI_DOUBLE,  MPI_MIN, MPI_COMM_WORLD);
  min_write_time = min_write_time + longest_io_time;

  

  
  if (my_rank % 100 == 0)
    printf("Write time      on rank %d, sum = %f, ave = %f s \n", my_rank, write_time, write_time/ite);


  //Todo: we need method to print corret information when fake_error_flag==1
  if (my_rank == 0 && fake_error_flag == 0) {
    
    printf("=====================================\n");
    printf("      Summary Information \n");
    printf("=====================================\n");

    printf("Total files        = %d \n", ite);
    printf("Total read time (max)    = %f seconds, ave = %f \n", read_time_all,  read_time_all/ite);
    printf("Total write time (max)   = %f seconds, ave = %f \n", write_time_all, write_time_all/ite);
    printf("Total read time (min)    = %f seconds, ave = %f \n", min_read_time,  min_read_time/ite);
    printf("Total write time (min)   = %f seconds, ave = %f \n", min_write_time, min_write_time/ite);
    printf("Total size         = %f GB\n", data_size_all);
    printf("Average read  rate = %f GB/s\n", data_size_all/read_time_all);
    printf("Average write rate = %f GB/s\n", data_size_all/write_time_all);
    printf("Update metadata on rank 0, sum = %f, ave = %f s \n", metadata_time, metadata_time/ite );
    printf("Write time      on rank 0, sum = %f, ave = %f s \n", rank0_write_time, rank0_write_time/ite);
    printf("=====================================\n");


    if (result_file_flag == 1){
      fprintf(rfp, "=====================================\n");
      fprintf(rfp, "      Summary Information \n");
      fprintf(rfp, "=====================================\n");
      
      fprintf(rfp, "Total files        = %d \n", ite);
      fprintf(rfp, "Total read time (max)    = %f seconds, ave = %f \n", read_time_all,  read_time_all/ite);
      fprintf(rfp, "Total write time (max)   = %f seconds, ave = %f \n", write_time_all, write_time_all/ite);
      fprintf(rfp, "Total read time (min)    = %f seconds, ave = %f \n", min_read_time,  min_read_time/ite);
      fprintf(rfp, "Total write time (min)   = %f seconds, ave = %f \n", min_write_time, min_write_time/ite);
      fprintf(rfp, "Total size         = %f GB\n", data_size_all);
      fprintf(rfp, "Average read  rate = %f GB/s\n", data_size_all/read_time_all);
      fprintf(rfp, "Average write rate = %f GB/s\n", data_size_all/write_time_all);
      fprintf(rfp, "Update metadata on rank 0, sum = %f, ave = %f s \n", metadata_time, metadata_time/ite );
      fprintf(rfp, "Write time      on rank 0, sum = %f, ave = %f s \n", rank0_write_time, rank0_write_time/ite);
      fprintf(rfp, "=====================================\n");
      fclose(rfp);
    }
  }
  
  MPI_Finalize();
  exit(0);
}

//
// Wait next available file to move
//
int next_file(char *bb_fn, char *dk_fn){
  DEMetaRecord d2bmetast;
  int ret;

  while(1){
    ret = NextBBDoneRecord(&d2bmetast);
    if(ret == 0){
      if((strcmp(d2bmetast.dkname, "NULL")==0) && (strcmp(d2bmetast.bbname, "NULL") == 0)){
        strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
        strncpy(bb_fn, d2bmetast.bbname, sizeof(d2bmetast.bbname));
        return 2; //End of simulation
      }
    
      strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
      strncpy(bb_fn, d2bmetast.bbname, sizeof(d2bmetast.bbname));
      return 0;
    }
  
    if(ret == -1)
      return -1;

    //Since there is no new file coming
    //do check app_failed_flag to see wether we should exit
    if (app_failed_flag == 1){
      strncpy(dk_fn, "NULL", sizeof("NULL"));
      strncpy(bb_fn, "NULL", sizeof("NULL"));
      return 0;
    }
    sleep(MONITOR_INTERVAL);
  }
}

int file_exist_b2d (char *filename)
{
  struct stat   buffer;   
  return (stat (filename, &buffer) == 0);
}

void set_striping_size(char *fn, MPI_Offset ss){
   char command[NAME_LENGTH] = {0};
   int  sc = OST_NUMS;
   
   //MPI_File_delete(fn, MPI_INFO_NULL);
   if(file_exist_b2d(fn))
     remove(fn);
   
   sprintf(command, "lfs setstripe ----stripe-size %llu --count %d  %s \n", ss, sc, fn);
   //system(command);
   FILE *fp;
   fp = popen(command, "r");
   if (fp == NULL) {
     printf("Failed to run command [%s], but we still continue to run the code !\n", command );
     //exit(1);
   }
   char path[NAME_LENGTH];
   while (fgets(path, sizeof(path)-1, fp) != NULL) {
     printf("%s", path);
   }
   pclose(fp);
   
   /*
   sprintf(command, "lfs getstripe %s \n", fn);
   //system(command);
   fp = popen(command, "r");
   if (fp == NULL) {
	   printf("Failed to run command [%s]\n", command );
	   exit(1);
   }
   sprintf(command, "lfs getstripe %s \n", fn);
   while (fgets(path, sizeof(path)-1, fp) != NULL) {
	printf("%s", path);
   }
   pclose(fp); */

}



#include "record-op-file.h"
#include "db-file.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>     /* MPI and MPI-IO live here */
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>  /* getopt lives here */

char    de_mdb_name_str[NAME_LENGTH];

int CreateDB(){
  int my_rank;
  static int create_flag = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  //printf("In create db file !\n");
  if (my_rank == 0 && create_flag == 0){
    //DEMetaRecord d2bmetast;
    // Create database
    mdt_create(de_mdb_name_str, sizeof(DEMetaRecord));
    //db_CloseAll();
    //mdt_close();
    create_flag = 1;
    printf("Create Data Elevator metadat table [%s]\n", de_mdb_name_str);
  }
}

//return 
//1 file exists
//0 file does not exist
int DBExistingCheck(){
  //int me;
  //MPI_Comm_rank(MPI_COMM_WORLD, &me);
  //printf("At rank %d: db name= %s \n", me, de_mdb_name_str);
  return file_exist(de_mdb_name_str);
}

int NameDB(){
  int my_rank;
  static int updated_flag = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  //Use root (rank == 0) process to update name once
  //Comment this line for netcdf test
  //if (my_rank == 0 && updated_flag == 0){
  if (updated_flag == 0){
    const char *bb_path = getenv("DW_JOB_STRIPED");
    if(bb_path!=NULL){
      sprintf(de_mdb_name_str, "%s/%s", bb_path, DE_DB_NAME);
    }else{
      sprintf(de_mdb_name_str, "./%s", DE_DB_NAME);
    }
    
    if(my_rank == 0) printf("Naming the metadata table: %s \n", de_mdb_name_str);
    updated_flag  = 1;
  }
}

int AppendRecord(char *fnd, char *fnb, int status, int size){
  // Open database
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    sprintf(d2bmetast.status, "%d", DE_NEW_FILE_BB);
    sprintf(d2bmetast.size,   "%d", 0);
    strncpy(d2bmetast.bbname, fnb, sizeof(d2bmetast.bbname));
    strncpy(d2bmetast.dkname, fnd, sizeof(d2bmetast.dkname));
    //printf("append: bb name %s, dk name %s \n", d2bmetast.bbname, d2bmetast.dkname);
    printf("Rediect disk file [%s] to temp BB file [%s] by Data Elevator \n", fnd, fnb);
    mdt_ap_record(&d2bmetast);
    mdt_close();
  }
}

int AppendReadRecord(char *fnd, char *fnb, char *groupname, char *dsetname, int status, int size){
  // Open database
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    //db_Open(b2d_db_name_str, "b2ddb");
    //D2BMetaSt d2bmetast;
    sprintf(d2bmetast.status, "%d", DE_NEW_FILE_BB);
    sprintf(d2bmetast.size,   "%d", 0);
    strncpy(d2bmetast.bbname, fnb, sizeof(d2bmetast.bbname));
    strncpy(d2bmetast.dkname, fnd, sizeof(d2bmetast.dkname));
    strncpy(d2bmetast.groupname, groupname, sizeof(d2bmetast.groupname));
    strncpy(d2bmetast.dsetname,  dsetname,  sizeof(d2bmetast.dsetname));
    //printf("append: bb name %s, dk name %s \n", d2bmetast.bbname, d2bmetast.dkname);
    printf("Rediect read disk file [%s] to bb file [%s] by B2D \n", fnd, fnb);
    mdt_ap_record(&d2bmetast);
    mdt_close();
  }
}


char *AppendRecordVol(char *fnd, int status, int size){
  // Open database
  int   my_rank;
  char *fnb = (char *)malloc(sizeof(char)*NAME_LENGTH);
  char *ts2 = strdup(fnd);
  char *filename = basename(ts2);
  const char *bb_path = getenv("DW_JOB_STRIPED");
  if(bb_path!=NULL){
    sprintf(fnb, "%s/%s.on.bb", bb_path, filename);
  }else{
    sprintf(fnb, "./%s.on.bb", filename);
  }
  
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    sprintf(d2bmetast.status, "%d", DE_NEW_FILE_BB);
    sprintf(d2bmetast.size,   "%d", 0);
    strncpy(d2bmetast.bbname, fnb, sizeof(d2bmetast.bbname));
    strncpy(d2bmetast.dkname, fnd, sizeof(d2bmetast.dkname));
    printf("Rediect disk file [%s] to bb file [%s] by Data Elevator \n", fnd, fnb);
    //printf("append new record: bb name %s, dk name %s \n", d2bmetast.bbname, d2bmetast.dkname);
    mdt_ap_record(&d2bmetast);
    mdt_close();
  }

  return fnb;
}

int UpdateRecord(char *fnd, int type, char *value){
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    //db_Open(b2d_db_name_str, "b2ddb");
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    mdt_goto_first_record();
    do{
      mdt_read_crecord(&d2bmetast);
      //printf("1 fnd = %s, fn =  %s \n", fnd, d2bmetast.dkname);
      if((strcmp(d2bmetast.dkname, fnd) == 0)){
        switch(type){
          case DE_TYPE_K:
            strncpy(d2bmetast.dkname,  value, sizeof(d2bmetast.dkname));
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_S:
            strncpy(d2bmetast.status, value, sizeof(d2bmetast.status));
            //printf("In UpdateRecord: fn =  %s, Status = %s \n", d2bmetast.dkname, value);
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_B:
            strncpy(d2bmetast.bbname,  value, sizeof(d2bmetast.bbname));
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_E:
            strncpy(d2bmetast.size,   value, sizeof(d2bmetast.size));
            mdt_write_crecord(&d2bmetast);
            break;
          default:
            printf("Unknow type in QueryRecord ! \n");
            break;
        }
      }
    }while(mdt_goto_next_crecord() != MDB_FALSE);
    
    mdt_close();
  }
  return 0;
  
}

int UpdateRecordVOL(char *fnb, int type, char *value){
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    mdt_goto_first_record();
    do{
      mdt_read_crecord(&d2bmetast);
      //printf("1 fnd = %s, fn =  %s \n", fnd, d2bmetast.dkname);
      if((strcmp(d2bmetast.bbname, fnb) == 0)){
        switch(type){
          case DE_TYPE_K:
            strncpy(d2bmetast.dkname,  value, sizeof(d2bmetast.dkname));
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_S:
            strncpy(d2bmetast.status, value, sizeof(d2bmetast.status));
            //printf("In UpdateRecord: fn =  %s, Status = %s \n", d2bmetast.dkname, value);
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_B:
            strncpy(d2bmetast.bbname,  value, sizeof(d2bmetast.bbname));
            mdt_write_crecord(&d2bmetast);
            break;
          case DE_TYPE_E:
            strncpy(d2bmetast.size,   value, sizeof(d2bmetast.size));
            mdt_write_crecord(&d2bmetast);
            break;
          default:
            printf("Unknow type in QueryRecord ! \n");
            break;
        }
      }
    }while(mdt_goto_next_crecord() != MDB_FALSE);
    
    mdt_close();
  }
  return 0;
}

int  QueryRecord(char *fnd, int type, char *value){
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    mdt_goto_first_record();
    do{
      mdt_read_crecord(&d2bmetast);
      if(strcmp(d2bmetast.dkname, fnd) == 0){
        switch(type){
          case DE_TYPE_K:
            strncpy(value, d2bmetast.dkname, sizeof(d2bmetast.dkname));
            break;
          case DE_TYPE_S:
            strncpy(value, d2bmetast.status, sizeof(d2bmetast.status));
            break;
          case DE_TYPE_B:
            strncpy(value, d2bmetast.bbname, sizeof(d2bmetast.bbname));
            break;
          case DE_TYPE_E:
            strncpy(value, d2bmetast.size,   sizeof(d2bmetast.size));
            break;
          default:
            printf("Unknow type in QueryRecord ! \n");
            break;
        }
      }
    }while(mdt_goto_next_crecord() != MDB_FALSE);
    
    mdt_close();
  }
  return 0;
}

//return 
//1 file exists
//0 file does not exist
int file_exist(char *filename)
{
  struct stat  buffer;   
  return (stat(filename, &buffer) == 0);
}

// return values
//  0: find some finished record
//  1: no one is finished (or no record) 
// -1: some error happens
int  NextBBDoneRecord(DEMetaRecord *d2bmetast){
  //D2BMetaSt d2bmetast;
  int status, ret=1;
  if(file_exist(de_mdb_name_str) != 1){
    return ret;
  }

  mdt_open(de_mdb_name_str);
  
  int s = mdt_get_amount();
  if (s==0 ){
    mdt_close();
    return ret;
  }
  
  mdt_goto_first_record();
  do{
    mdt_read_crecord(d2bmetast);
    status = atoi(d2bmetast->status);
    //printf("In NextBBDoneRecord -> Status id = %d \n", status);
    if(status == DE_WRITE_BB_DONE){
      ret = 0;
      break;
    }
  }while(mdt_goto_next_crecord() != MDB_FALSE);
  
  mdt_close();
  
  return ret;
}




// return values
//  1: find the file, and bbname contains the temp tile's name on BB
//  0: no 
int  DBRecordExistCheck(char *dkname, char *bbname){
  DEMetaRecord d2bmetast;
  int status, ret=0;
  if(file_exist(de_mdb_name_str) != 1){
    return ret;
  }
  mdt_open(de_mdb_name_str);
  int s = mdt_get_amount();
  if (s==0 ){
    mdt_close();
    return ret;
  }
  
  mdt_goto_first_record();
  do{
    mdt_read_crecord(&d2bmetast);
    //status = atoi(d2bmetast->status);
    //printf("In NextBBDoneRecord -> Status id = %d \n", status);
    if( strcmp(d2bmetast.dkname, dkname) ==  0){
      ret = 1;
      //printf("Found the name : %s \n",d2bmetast.bbname);
      //bbname = strdup(d2bmetast.bbname);
      strcpy(bbname, d2bmetast.bbname);
      break;
    }
  }while(mdt_goto_next_crecord() != MDB_FALSE);
  
  mdt_close();
  
  return ret;
}


void AddMPIFileHandle(char *fn, unsigned long long fh){
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    mdt_goto_first_record();
    do{
      mdt_read_crecord(&d2bmetast);
      printf("In Add Handle: target file handle = %lld, target fn =  %s, current fs=%s \n", fh, fn,  d2bmetast.dkname);
      if(strcmp(d2bmetast.bbname, fn) == 0){
        d2bmetast.mpi_file_handle = fh;
        mdt_write_crecord(&d2bmetast);
        break;
      }
    }while(mdt_goto_next_crecord() != MDB_FALSE);
    mdt_close();
  }
}


int SetMPIFileClose(unsigned long long fh){
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  if (my_rank == 0){
    mdt_open(de_mdb_name_str);
    DEMetaRecord d2bmetast;
    mdt_goto_first_record();
    do{
      mdt_read_crecord(&d2bmetast);
      printf("Set close  fh = %lld, fn =  %s, current fh %lld \n", fh, d2bmetast.dkname, d2bmetast.mpi_file_handle);
      if(d2bmetast.mpi_file_handle  ==  fh ){
        char  value[NAME_LENGTH];
        //printf("update status for %s with %d \n", bb_filename,B2D_WRITE_BB_DONE);
        sprintf(value, "%d", DE_WRITE_BB_DONE);
        strncpy(d2bmetast.status, value, sizeof(d2bmetast.status));
        printf("In UpdateRecord: fn =  %s, Status = %s \n", d2bmetast.dkname, value);
        mdt_write_crecord(&d2bmetast);
        break;
      }
    }while(mdt_goto_next_crecord() != MDB_FALSE);
    mdt_close();
  }
  return 0;
}

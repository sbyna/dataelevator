#ifndef    DE_RECORD_OP_H
#define    DE_RECORD_OP_H


#include "de-error.h"

//#define B2D_NEW_FILE_BB     000
//#define B2D_WRITE_BB        001
//#define B2D_WRITE_BB_DONE   010
//#define B2D_NEW_FILE_DISK   011
//#define B2D_WRITE_DISK      100
//#define B2D_WRITE_DISK_DONE 101

#define DE_NEW_FILE_BB     0
#define DE_WRITE_BB        1
#define DE_WRITE_BB_DONE   2
#define DE_NEW_FILE_DISK   3
#define DE_WRITE_DISK      4
#define DE_WRITE_DISK_DONE 5


#define DE_TYPE_K          0   //key value (disk file name)
#define DE_TYPE_S          1   //status
#define DE_TYPE_B          2   //burst buffer file name
#define DE_TYPE_E          3   //size

#define DE_DB_NAME        "DataElevatorMetaTable.db"

#define NAME_LENGTH       512 
#define MAX_DIMS   20

extern char de_db_name_str[NAME_LENGTH];
// Define client structure (register)
typedef struct
{
  char   dkname[NAME_LENGTH]; //key of the record
  char   bbname[NAME_LENGTH];
  char   status[NAME_LENGTH];
  char     size[NAME_LENGTH];
  char groupname[NAME_LENGTH];
  char dsetname[NAME_LENGTH];
  int  rank;  //Rank of the dimension of the dsetname 
  unsigned long long dims[MAX_DIMS]; //Size of each dimension of the dsetname
  unsigned long long chks[MAX_DIMS]; //Size of chunk size for each diemnsion of the dsetname
  char     chkbitmap[NAME_LENGTH];   //Existing bitmap
  unsigned long long mpi_file_handle;
} DEMetaRecord;

int CreateDB();

//Function on each record 
int      AppendRecord(char *fnd, char *fnb, int status, int size);
//Append the record for the read file
int     AppendReadRecord(char *fnd, char *fnb, char *groupname, char *dsetname, int status, int size);
//Append the record for written file    
char    *AppendRecordVol(char *fnd, int status, int size);
int      UpdateRecord(char *fnd, int type, char *value);
int   UpdateRecordVOL(char *fnb, int type, char *value);
int       QueryRecord(char *fnd, int type, char *value);
//Obtain next record with status == B2D_WRITE_BB_DONE
int  NextBBDoneRecord(DEMetaRecord *d2bmetast);
int  NameDB();
void AddMPIFileHandle(char *fn, unsigned long long fh);
int  SetMPIFileClose(unsigned long long fh);

#endif

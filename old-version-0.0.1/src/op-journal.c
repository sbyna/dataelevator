
#include <stdio.h>
#include <stdlib.h>
#include "op-journal.h"
#include "record-op-file.h"
#include <sys/stat.h>

//Return the full path /w name to the log
char *log_file_pathname(char *fname){
  char    *log_file_pathname = (char *)malloc(sizeof(char) * NAME_LENGTH);
  //const char *bb_path = getenv("DW_JOB_STRIPED");
  sprintf(log_file_pathname, "%s.delog", fname);
  return log_file_pathname;
}

//Create a new file to record moved chunks
//The log file is created when user specify -j to detamovement job
//The log file is created only when no file with the same name exists
int   create_log_file(char *fname){
  char    *lfname;
  lfname = log_file_pathname(fname);
  FILE *fp;
  fp = fopen(lfname, "w+");
#ifdef DEBUG
  printf("Create the log file: %s \n", lfname);
#endif
  //Put zero to be there
  int num =  0;
  fwrite(&num, sizeof(int), 1, fp);
  fflush(fp);
  fclose(fp);
}

//Delete the log file after the file movement is finished
int   delete_log_file(char *fname){
  char    *lfname;
  lfname = log_file_pathname(fname);
  int ret = remove(lfname);
  if(ret == 0){
#ifdef DEBUG
    printf("HL File [%s] deleted successfully.\n", lfname);
#endif
  }else{
    printf("Error: unable to delete the log file: %s \n", lfname);
  }
  return ret;
}

//Determine the existing of the log file
// 1: file exists
// 0: file does not exist
int   check_log_file( char *fname){
  char    *lfname;
  lfname = log_file_pathname(fname);
  struct stat  buffer;   
  return (stat(lfname, &buffer) == 0);
}

//Update the status recroded in the log file
//Automatically increace by one.
//The actual contents record the iteration number of chunks,
//which are being moved by all processes of data movement job
int   update_log_file(char *fname){
  char    *lfname;
  FILE *fp;
  int num;
  
  lfname = log_file_pathname(fname);
  fp = fopen(lfname, "w+");
  fread(&num,  sizeof(int), 1, fp);
  num = num + 1;
  fwrite(&num, sizeof(int), 1, fp);
  fflush(fp);
  fclose(fp);
}

//Read the content of the log file
//return a interger, which is the last iteration #
int   read_log_file(char *fname){
  FILE *fp;
  char    *lfname;
  lfname = log_file_pathname(fname);

  fp = fopen(lfname, "w+");
  int num;
  fread(&num,  sizeof(int), 1, fp);
  fclose(fp);
  return num;
}






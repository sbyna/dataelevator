#!/bin/bash
cp_nodes_num=1
cores_per_node=1
job_commd="srun"
dejob_path="./"

trap - SIGINT

#trap '{ echo "Hey, you pressed Ctrl-C to stop $0.  Time to quit." ; exit 1; }' INT

DEJOB_OPTS=""
usage(){
    echo "Usage: $0 c:r:ijdahsmN:n:" 1>&2
    exit 1
}

while getopts "c:r:ijfdahsmN:n:b:p:" opt; do
  case "${opt}" in
      d)
	  DEJOB_OPTS+=" -d "
	  ;;
      r)
	  DEJOB_OPTS+=" -r "
	  DEJOB_OPTS+=${OPTARG}
	  ;;
      m)
	  DEJOB_OPTS+=" -m "
	  ;;
      i)
	  DEJOB_OPTS+=" -i "
	  ;;
      a)
	  DEJOB_OPTS+=" -a "
	  ;;
      c)
	  DEJOB_OPTS+=" -c "
	  DEJOB_OPTS+=${OPTARG}
	  ;;
      f)
	  DEJOB_OPTS+=" -f "
	  ;;
      j)
	  DEJOB_OPTS+=" -j "
	  ;;
      s)
	  DEJOB_OPTS+=" -s "
	  ;;
      N)
	  cp_nodes_num = ${OPTARG}
	  ;;
      n)
	  cores_per_node = ${OPTARG}
	  ;;
      b)
	  job_commd=${OPTARG}
	  ;;
      p)
	  dejob_path=${OPTARG}
	  ;;
      *)
	  usage
	  ;;
  esac
done

while true; do
    #The job will be restarted once it fail
    echo "Start DEJob with ${cp_nodes_num} (${cores_per_node} cores/node), job command is ${job_commd} ..."
    if [ "$job_commd" == "mpirun" ]; then   
	${job_commd} -n ${cores_per_node} ${dejob_path}/dejob ${DEJOB_OPTS}
    else
        ${job_commd} -N ${cp_nodes_num} -n ${cores_per_node} ${dejob_path}/dejob ${DEJOB_OPTS} 
    fi
    exitcode=$?
    if [[ $exitcode == 0 ]]; then
	break ##Return without any error
    else
	echo "   --------Notice-----------------------------------|  "
	echo "   |Job failed for somereson. Anyhow, we restart it |  "
	echo "   -------------------------------------------------|  "
    fi	
done


exit 0


#ifndef    DE_OP_JOURNAL_H
#define    DE_OP_JOURNAL_H

//Return the full path /w name to the log
char *log_file_pathname(char *fname);

//Create a new file to record moved chunks
//The log file is created when user specify -j to detamovement job
//The log file is created only when no file with the same name exists
int   create_log_file(char *fname);

//Delete the log file after the file movement is finished
int   delete_log_file(char *fname);

//Determine the existing of the log file
// 1: file exists
// 0: file does not exist
int   check_log_file( char *fname);

//Update the status recroded in the log file
//Automatically increace by one.
//The actual contents record the iteration number of chunks,
//which are being moved by all processes of data movement job
int   update_log_file(char *fname);

//Read the content of the log file
//return a interger, which is the last iteration #
int   read_log_file(char *fname);

#endif

#ifndef DE_JOB_H
#define DE_JOB_H
#include <stdio.h>

//Time interval to minoter metadata table
#define MONITOR_INTERVAL   2
//The number of OSTes in Lustre
#define OST_NUMS           248 
//The striping size to read/write data
#define BASIC_REQUEST_SIZE 8388608  //8 MB
//The max number of iterations to read/write data
#define MAX_ITERATION      1000000

int  next_file(char *bb_fn, char *dk_fn);
void set_striping_size(char *fn, MPI_Offset ss);

#endif

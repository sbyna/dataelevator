#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 2
#SBATCH -t 00:10:00
#SBATCH -J de-test-bb-write
#SBATCH -C haswell
#SBATCH --output de-test-bb-write.%j.out
#SBATCH --error  de-test-bb-write.%j.err
#DW jobdw capacity=10GB access_mode=striped type=scratch

cd $SLURM_SUBMIT_DIR

srun -n 2  -N 1  ./test &
srun  -n 2 -N 1  ../build/bin/dejob -a -i -m 

wait

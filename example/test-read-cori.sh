#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N 2
#SBATCH -t 00:10:00
#SBATCH -J de-test-bb-write
#SBATCH -C haswell
#SBATCH --output de-test-bb-read.%j.out
#SBATCH --error  de-test-bb-read.%j.err
###DW jobdw capacity=10GB access_mode=striped type=scratch

cd $SLURM_SUBMIT_DIR

export HDF5_USE_FILE_LOCKING=FALSE


rm DataElevatorMetaTable.db
rm -rf prefetch-100by100.h5-de-dir

srun  -n 2  -N 1  /global/homes/d/dbin/work/dataelevator-release/example/test -p &
srun  -n 2  -N 1  /global/homes/d/dbin/work/dataelevator-release/build/bin/dejob -p -n 2 -t 10,10 -g 0,0 -i 

wait

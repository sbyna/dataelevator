
#!/bin/sh
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 write or prefetch" >&2
  exit 1
fi

if [ "$1" == "write" ]; then
	mpirun -n 2 ../build/bin/dejob -a -i -m
elif [ "$1" == "prefetch" ]; then
	mpirun -n 2 ../build/bin/dejob -p -n 2 -t 10,10 -g 0,0 -i	
else
	echo "wrong paramter [$s1], please try write/prefetch"
fi



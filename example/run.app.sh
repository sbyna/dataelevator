

#!/bin/sh
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 write or prefetch" >&2
  exit 1
fi

rm DataElevatorMetaTable.db
rm h5file-0.h5.on.bb
rm h5file-1.h5.on.bb
rm h5file-2.h5.on.bb
rm h5file-0.h5
rm h5file-1.h5
rm h5file-2.h5
rm -rf prefetch-100by100.h5-de-dir



if [ "$1" == "write" ]; then
	mpirun -n 2 ./test
elif [ "$1" == "prefetch" ]; then
	mpirun -n 2 ./test -p
else
	echo "wrong paramter [$s1], please try write/prefetch"
fi


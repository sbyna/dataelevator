#!/bin/bash
de_nodes=1
de_cores_per_node=1
app_nodes=1
app_cores_per_node=1
job_run_commd="srun"
app_exe="./simjob"
dejob_path="./"

trap - SIGINT

#trap '{ echo "Hey, you pressed Ctrl-C to stop $0.  Time to quit." ; exit 1; }' INT

DEJOB_OPTS=""
usage(){
    echo "Usage: $0 c:r:ijdahsmN:n:x:y:" 1>&2
    exit 1
}

APP_OPTS=""

while getopts "c:r:ijfdahsmN:n:b:p:x:y:a:e:o:" opt; do
  case "${opt}" in
      d)
	  DEJOB_OPTS+=" -d "
	  ;;
      r)
	  DEJOB_OPTS+=" -r "
	  DEJOB_OPTS+=${OPTARG}
	  ;;
      m)
	  DEJOB_OPTS+=" -m "
	  ;;
      i)
	  DEJOB_OPTS+=" -i "
	  ;;
      a)
	  DEJOB_OPTS+=" -a "
	  ;;
      c)
	  DEJOB_OPTS+=" -c "
	  DEJOB_OPTS+=${OPTARG}
	  ;;
      f)
	  DEJOB_OPTS+=" -f "
	  ;;
      j)
	  DEJOB_OPTS+=" -j "
	  ;;
      s)
	  DEJOB_OPTS+=" -s "
	  ;;
      N)
	  ap_nodes = ${OPTARG}
	  ;;
      n)
	  app_cores_per_node = ${OPTARG}
	  ;;
      o)
	  APP_OPTS=${OPTARG}
	  ;;
      x)
	  de_nodes = ${OPTARG}
	  ;;
      y)
	  de_cores_per_node = ${OPTARG}
	  ;;
      b)
	  job_commd=${OPTARG}
	  ;;
      p)
	  dejob_path=${OPTARG}
	  ;;
      e)
	  app_exe=${OPTARG}
	  ;;
      *)
	  usage
	  ;;
  esac
done

app_pid=0
de_pid=0

#The job will be restarted once it fail
printf "++ Start applications job for ${app_exe} with  ${app_nodes} (${app_cores_per_node} cores/node).
           Its argument list: ${APP_OPTS}\n
        ++Start Data Elevator's job with ${de_nodes} (${de_cores_per_node} cores/node).
           Its argument list: ${DEJOB_OPTS} \n
        ++Command to run jobs is ${job_commd} ... \n"
if [ "$job_commd" == "mpirun" ]; then   
    ${job_commd} -n ${app_nodes} -ppn ${app_cores_per_node}  ${app_exe} ${APP_OPTS}   & app_pid=("$!")
    ${job_commd} -n ${de_nodes}  -ppn ${de_cores_per_node}   ${dejob_path}/dejob ${DEJOB_OPTS} & de_pid=("$!")
else
    ${job_commd} -N ${app_nodes} -n ${app_cores_per_node}  ${app_exe} ${APP_OPTS}            & app_pid=("$!")
    ${job_commd} -N ${de_nodes}  -n ${de_cores_per_node}  ${dejob_path}/dejob ${DEJOB_OPTS}  & de_pid=("$!")
fi

echo ${app_pid}
echo  ${de_pid}


##https://stackoverflow.com/questions/1570262/shell-get-exit-code-of-background-process
while true; do
    sleep 1
    if ! ps -p ${app_pid} >/dev/null ; then
        wait ${app_pid}
	exitcode=$?
	if [[ $exitcode != 0 ]]; then
	    echo "   --------Notice-----------------------------------  "
	    echo "   |Application Job failed for some reason.           "
            echo "   |Anyhow, we need to terminate Data Elevator job    "
	    echo "   -------------------------------------------------  "
	    kill -s SIGUSR1 ${de_pid}
	fi	
        ##echo  "Application(pid:${app_pid}):exited($?); "
    else
        echo  "Application(pid:${app_pid}):running; "
    fi
    
    if ! ps -p ${de_pid} >/dev/null ; then
        wait ${de_pid}
	exitcode=$?
	if [[ $exitcode == 0 ]]; then
	    break ##Return without any error
	else
	    echo "   --------Notice--------------------------------------|  "
	    echo "   |DE Job failed for some reasons. We restart it      |  "
	    echo "   ----------------------------------------------------|  "
	    if [ "$job_commd" == "mpirun" ]; then   
		${job_commd} -n ${de_nodes}  -ppn ${de_cores_per_node}   ${dejob_path}/dejob ${DEJOB_OPTS} & de_pid=("$!")
	    else
		${job_commd} -N ${de_nodes}  -n ${de_cores_per_node}  ${dejob_path}/dejob ${DEJOB_OPTS}  & de_pid=("$!")
	    fi
	fi	
    else
        echo  "DataElevator(pid:${de_pid}):running; "
    fi
done

wait ${app_pid}
wait ${de_pid}

exit 0


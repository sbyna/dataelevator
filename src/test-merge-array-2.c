    #include "merge-array.h"


    #define RANK_A_1 8
    #define RANK_A_2 8

    #define RANK_B_1 3
    #define RANK_B_2 3

  int main(){
   float *A =  malloc(sizeof(float) * RANK_A_1 * RANK_A_2);
   unsigned long long va_A_start_coordinate[2]={1,1};
   unsigned long long va_A_count[2]={RANK_B_1,RANK_B_2};
   unsigned long long va_A_dims[2]={RANK_A_1,RANK_A_2};

   int i,j;

   float temp = 0;
   printf("A = : \n");
   for(i=0; i<RANK_A_1; i++) {
    for(j=0;j<RANK_A_2;j++) {
     A[i * RANK_A_2+ j] = temp;
     temp++;
     printf("%f, ", A[i*RANK_A_2 + j]); 
   }
   printf("\n");
  }




  float *B = malloc(sizeof(float)*RANK_B_1 * RANK_B_2);
  unsigned long long va_B_start_coordinate[2]={0,0};
  unsigned long long va_B_count[2]={RANK_B_1,RANK_B_2};
  unsigned long long va_B_dims[2]={RANK_B_1,RANK_B_2};

  printf("B = : \n");
  for(i=0; i<RANK_B_1; i++) {
    for(j=0;j<RANK_B_2;j++) {
     printf("%f, ", B[i*RANK_B_2+j]); 
   }
   printf("\n");
  }


  VArray_copy(2, sizeof(float), va_A_dims, va_A_start_coordinate, va_A_count, A, va_B_dims, va_B_start_coordinate, va_B_count, B, 0);
  



  printf("B (after merge) = : \n");
  for(i=0; i<RANK_B_1; i++) {
    for(j=0;j<RANK_B_2;j++) {
     printf("%f, ", B[i*RANK_B_2+j]); 
     B[i*RANK_B_2+j]++;
   }
   printf("\n");
  }


  VArray_copy(2, sizeof(float), va_A_dims, va_A_start_coordinate, va_A_count, A, va_B_dims, va_B_start_coordinate, va_B_count, B, 1);

  printf("A (after merge back ++) = : \n");
  for(i=0; i<RANK_A_1; i++) {
    for(j=0;j<RANK_A_2;j++) {
      printf("%f, ", A[i*RANK_A_2 + j]); 
    }
    printf("\n");
  }


}
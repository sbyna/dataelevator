#ifndef __KVS_H__
#define __KVS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
  

  typedef unsigned long long kt;
  typedef char           *   vt;
typedef struct {
  kt key;
  vt value;
} KVSpair;

typedef struct {
    KVSpair *pairs;
    size_t length;
} KVSstore;

KVSstore *kvs_create(void);

void kvs_destroy(KVSstore *store);

void kvs_put(KVSstore *store,  kt key, vt value);

vt kvs_get(KVSstore *store, kt key);

#ifdef __cplusplus
}
#endif

#endif /* #define __KVS_H__ */

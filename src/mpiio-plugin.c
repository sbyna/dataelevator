#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include "mpio.h"
#include "record-op-file.h"
#include <stdlib.h>
#include "memkv.h"
//#include "mpioimpl.h"
//#include "adio.h"

int MPI_File_open(MPI_Comm comm, const char *filename, int amode, MPI_Info info, MPI_File *fh){
    int me;
    PMPI_Comm_rank(comm, &me);
    if(me == 0){
      printf("In PMPI: Open file name is : %s \n", filename);
      fflush(stdout);
    }
    char *new_name, *temp_name; int open_new_file_flag = 0;
    NameDB(); //On all ranks
    if(DBExistingCheck() == 1){ //On all ranks
      temp_name = malloc(NAME_LENGTH); memset(temp_name, 0, NAME_LENGTH);
      if(DBRecordExistCheck(filename, temp_name) ==  1){  //On all ranks
        //printf("At rank %d : In PMPI: replace [%s] with [%s] for file open. \n", me, filename, new_name);
        //When DB exists and also the file record exists
        //We assume there is a read to happen right after write to BB
        //In this case, Data Elevator runs with "-m " flag 
        //Todo: we can also check amode and info to have a double check
        new_name = malloc(NAME_LENGTH); memset(new_name, 0, NAME_LENGTH);
        strcpy(new_name, temp_name);  
        open_new_file_flag = 1;
        free(temp_name);
        //file_name_ptr = strdup(new_name); //Record for file close
      }else{
        //When DB exists but file record does not exists
        // We create a ew record here
        //Todo: HDF5 might be writing the file
        new_name = AppendRecordVol(filename, DE_NEW_FILE_BB, 0);
        open_new_file_flag = 1;
      }
    }else{
      //The first file is coming 
      CreateDB(); //Create DB on Disk
      new_name = AppendRecordVol(filename, DE_NEW_FILE_BB, 0);
      open_new_file_flag = 1;
      //new_file_name_ptr = strdup(new_name); //Record for file close
      //MPI_Info_set(info, "filename", filename);
    }

    int rt;
    if(open_new_file_flag == 1){
      rt = PMPI_File_open(comm, new_name, amode, info, fh);
      if(me == 0) printf("FD in open: %lld at rank %d, new_name = %s \n", *fh, me, new_name);
      AddMPIFileHandle(new_name, *fh);
      free(new_name);
      return rt;
    }else{
      rt=PMPI_File_open(comm, filename, amode, info, fh);
      if(me == 0) printf("FD in open: %lld at rank %d \n", *fh, me);
      return rt;
    }
}


int MPI_File_close(MPI_File *fh){
  int me, status;
  //Mark the file (denoted with *fh) is closed
  SetMPIFileClose(*fh);
  PMPI_Comm_rank(MPI_COMM_WORLD, &me);
  if(me == 0 ){
    printf("MPI_File handle's size : %d , value = %lld\n", sizeof(*fh), *fh);
    fflush(stdout);
  }

  status = PMPI_File_close(fh);
  return status;
}

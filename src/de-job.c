#include <stdio.h>  /* all IO stuff lives here */
#include <stdlib.h> /* exit lives here */
#include <unistd.h> /* getopt lives here */
#include <string.h> /* strcpy lives here */
#include <limits.h> /* INT_MAX lives here */
#include <mpi.h>    /* MPI and MPI-IO live here */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <tinydir.h>
#include "de-job.h"
#include "record-op-file.h"
#include "op-journal.h"
#include "hdf5.h"
#include <fcntl.h>

//#define DW_API
//#ifdef  DW_API
//#include <datawarp.h>
//#endif

int app_failed_flag = 0;

void my_handler(int signum)
{
  printf("Data Elevator: Received SIGUSR1 from application to terminate ! \n");
  if (signum == SIGUSR1)
  {
    app_failed_flag = 1;
  }
}

//#define DEBUG 1

#define MASTER_RANK 0
#define TRUE 1
#define FALSE 0
#define BOOLEAN int
#define MBYTE 1048576.0
#define GBYTE 1024 * 1024 * 1024
#define SYNOPSIS printf("synopsis: %s -f <file>\n", argv[0])

void printf_help(char *cmd)
{
  char *msg = "Usage: %s [OPTION] \n\
      	  -h help (--help)\n\
          -a overlap reading and writing \n\
          -c x: chunk size = x * 8MB \n\
          -d use DataWarp API (disabled by default) \n\
          -i independent MPI-IO  \n\
          -s set striping size to be chunk size  \n\
          -m keep temp files on burst buffer  \n\
          -r log file        \n\
          Example: srun -n 128 %s -a -i -s -r result.log \n";

  fprintf(stdout, msg, cmd, cmd);
}

void convert_str_vector(int n, char *str, int *vector)
{
  int i;
  char *pch;
  char temp[NAME_LENGTH];

  if (n == 1)
  {
    vector[0] = atoi(str);
  }
  else
  {
    strcpy(temp, str);
    pch = strtok(temp, ",");

    i = 0;
    while (pch != NULL)
    {
      //printf("%s \n", pch);
      vector[i] = atoi(pch);
      pch = strtok(NULL, ",");
      i++;
    }
  }
  return;
}

int partial_prefetch_flag = 0;

int main(int argc, char *argv[])
{
  /* my variables */
  int my_rank, pool_size, last_guy, i, count, filename_length;
  int filename_output_length, *junk, file_open_error;
  BOOLEAN i_am_the_master = FALSE;

  char filename[NAME_LENGTH], filename_output[NAME_LENGTH], *read_buffer;
  char *rfilename;
  /* MPI_Offset is long long */
  MPI_Offset number_of_bytes, my_offset, my_current_offset, total_number_of_bytes, number_of_bytes_ll, max_number_of_bytes_ll;
  double data_size_all = 0;
  MPI_File fh, fho;
  MPI_Status status;
  double start, finish, read_time = 0, write_time = 0, longest_io_time, read_time_all = 0, write_time_all = 0, min_read_time = 0, min_write_time = 0, metadata_time = 0, rank0_write_time = 0, rank0_write_time_start, rank0_write_time_end, restart_time = 0, min_restart_time = 0, max_restart_time = 0;

  //Chunk size
  size_t chunk_size = BASIC_REQUEST_SIZE, chunk_temp; //8MB, basic chunk size
  //Result file pointer
  FILE *rfp;
  //Different flags variable
  int collective_io = 1;
  int async_io = 0;
  int set_striping_size_flag = 0;
  int result_file_flag = 0;
  int remove_file_flag = 1;
  int ops_journal_flag = 0; //record the ops.
  int fake_error_flag = 0;  //Fake error flag
  int prefetch_flag = 0;    //Fake error flag

  int array_rank;
  char ghost_size_str[NAME_LENGTH];
  char tile_size_str[NAME_LENGTH];
  int ghost_size_vector[DE_MAX_DIMS];
  unsigned long long tile_size_vector[DE_MAX_DIMS];

  int use_datawarp_stage_out = 0, build_index = 0, query_index = 0, use_datawarp_stage_in = 0;

  signal(SIGUSR1, my_handler);

  /* ACTION */
  extern char *optarg;
  int c;
  while ((c = getopt(argc, argv, "c:r:ijfdahsmpn:g:t:be:k:x")) != EOF)
    switch (c)
    {
    case 'x':
      partial_prefetch_flag = 1;
      break;
    case 'e':
      strcpy(filename, optarg);
      break;
    case 'k':
      strcpy(filename_output, optarg);
      break;
    case 'd':
      use_datawarp_stage_out = 1;
      break;
    case 'b':
      use_datawarp_stage_in = 1;
      break;
    case 'r':
      result_file_flag = 1;
      rfilename = strdup(optarg);
      break;
    case 'm':
      remove_file_flag = 0;
      break;
    case 'i':
      collective_io = 0;
      break;
    case 'a':
      async_io = 1;
      break;
    case 'c':
      chunk_temp = strtoul(optarg, NULL, 0); //chunk_temp < 4096
      chunk_size = chunk_size * chunk_temp;  //MB
      break;
    case 'j':
      ops_journal_flag = 1;
      break;
    case 'h':
      printf_help(argv[0]);
      return 0;
      break;
    case 's':
      set_striping_size_flag = 0;
      break;
    case 'f':
      fake_error_flag = 1;
      break;
    case 'p':
      prefetch_flag = 1;
      break;
    case 'n':
      array_rank = atoi(optarg);
      break;
    case 'g':
      strcpy(ghost_size_str, optarg);
      break;
    case 't':
      strcpy(tile_size_str, optarg);
      break;
    default:
      printf("Wrong option [%c] for %s \n", c, argv[0]);
      printf_help(argv[0]);
      MPI_Abort(MPI_COMM_WORLD, 1);
      break;
    } /* end of switch(c) */

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &pool_size);
  last_guy = pool_size - 1;
  if (my_rank == MASTER_RANK)
    i_am_the_master = TRUE;

  //Be default, we suggest users to use Data Elevator
  if (use_datawarp_stage_in == 1)
  {
#ifdef DW_API
    rank0_write_time_start = MPI_Wtime();
    int ret;
    ret = dw_stage_file_in(filename, filename_output);
    if (ret < 0)
    {
      printf("dw_stage_file_in failed \n");
      exit(-1);
    }
    printf("dw_stage_file_in [%s] ..... \n", filename);

    ret = dw_wait_file_stage(filename);
    rank0_write_time_end = MPI_Wtime();
    if (ret < 0)
    {
      printf("dw_wait_file_stage [%s] failed \n", filename);
      exit(-1);
    }
    else
    {
      printf("dw_wait_file_stage [%s] succeeds, time: %f \n", filename, (rank0_write_time_end - rank0_write_time_start));
    }
    fflush(stdout);
    return 0;
#else
    printf("No support to for DataWarp api ! \n ");
    return 0;
#endif
  }

  //Update the name (path) of the file storing metadata table
  if (my_rank == 0)
  {
    NameDB();
    if (result_file_flag == 1)
    {
      rfp = fopen(rfilename, "w");
      if (rfp == NULL)
      {
        printf("Error in open log file: %s \n", rfilename);
        exit(-1);
      }
      else
      {
        printf("Log file is: %s \n", rfilename);
      }
    }
    if (ops_journal_flag == 0)
    {
      printf("TIMB2D starts to work ! \n");
      if (result_file_flag == 1)
        fprintf(rfp, "TIMB2D starts to work ! \n");
    }
    else
    {
      printf("TIMB2D HL starts to work ! \n");
      if (result_file_flag == 1)
        fprintf(rfp, "TIMB2D HL starts to work ! \n");
    }
  }

  fflush(stdout);

  int rf, ite = 0, first_write, first_stage_out = 1;

  if (prefetch_flag == 0)
  {
    //if(my_rank == 0) printf("I am during the write function. \n");

    //Start to move data from BB to Disk
    while (1)
    {
      //Wait the next available file
      if (my_rank == 0)
      {
        rf = next_file(filename, filename_output);
        if (rf == -1)
          exit(-1);
      }
      MPI_Barrier(MPI_COMM_WORLD);
      MPI_Bcast(filename, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(filename_output, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);

      //Finish signal of the simulaiton job
      if (strcmp(filename, "NULL") == 0 && strcmp(filename_output, "NULL") == 0)
      {
        break;
      }

      if ((fake_error_flag == 1) && (ite == 2))
      {
        exit(-1);
      }

      //Check whether we has a log file

      start = MPI_Wtime();
      int last_moved_chunk_batch = 0;
      if (ops_journal_flag == 1)
      {
        if (check_log_file(filename) == 1)
        {
          //Some failure happes last time
          //We continues the operations from last_moved_chunk_batch
          //All process checks and get the same last_moved_chunk_batch
          last_moved_chunk_batch = read_log_file(filename);
        }
        else
        {
          //DataElevator is trasfering a new file
          //Ranks zero creates log file for operations
          if (my_rank == 0)
            create_log_file(filename);
        }
      }
      finish = MPI_Wtime();
      restart_time = restart_time + finish - start;

      //Be default, we suggest users to use Data Elevator
      if (use_datawarp_stage_out == 1)
      {
#ifdef DW_API
        rank0_write_time_start = MPI_Wtime();
        int ret;
        if (prefetch_flag == 0)
        {
          ret = dw_stage_file_out(filename, filename_output, DW_STAGE_IMMEDIATE);
        }
        else
        {
          ret = dw_stage_file_in(filename, filename_output);
        }
        if (ret < 0)
        {
          printf("dw_stage_file_out failed \n");
          exit(-1);
        }

        ret = dw_wait_file_stage(filename);
        rank0_write_time_end = MPI_Wtime();
        rank0_write_time = rank0_write_time + (rank0_write_time_end - rank0_write_time_start);

        if (ret < 0)
        {
          printf("dw_wait_file_stage [%s] failed \n", filename);
          exit(-1);
        }
        else
        {
          printf("dw_wait_file_stage [%s] succeeds, time: %f \n", filename, (rank0_write_time_end - rank0_write_time_start));
        }
        fflush(stdout);

        if (prefetch_flag == 0)
        {
          goto stage_out;
        }
        else
        {
          goto stage_in_exit;
        }
#else
        printf("No support to for DataWarp api ! \n ");
#endif
      }
      first_write = 1;

      //Open the file on BB
      file_open_error = MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      if (file_open_error != MPI_SUCCESS)
      {
        char error_string[BUFSIZ];
        int length_of_error_string, error_class;
        MPI_Error_class(file_open_error, &error_class);
        MPI_Error_string(error_class, error_string, &length_of_error_string);
        printf("%3d: %s\n", my_rank, error_string);
        MPI_Error_string(file_open_error, error_string, &length_of_error_string);
        printf("%3d: %s\n", my_rank, error_string);
        MPI_Abort(MPI_COMM_WORLD, file_open_error);
      }

      //Get the file size
      MPI_File_get_size(fh, &total_number_of_bytes);
#ifdef DEBUG
      printf("%3d: total_number_of_bytes = %lld\n", my_rank, total_number_of_bytes);
#endif
      double temp_size = temp_size + ((double)total_number_of_bytes) / MBYTE;
      temp_size = temp_size / 1024.0;
      data_size_all = data_size_all + temp_size; //To GB

      //update the striping size
      //number_of_bytes_ll = total_number_of_bytes / pool_size;
      //the stripe_size value must be a multiple of 64 KB (65536)
      MPI_Offset chunks_by64k, chunks_by64k_pp, basic_strip_size = 65536; //
      if (chunk_size > total_number_of_bytes / pool_size)
      {
        //Choose a multiple of 64KB as chunk size (=striping size)
        if (total_number_of_bytes % basic_strip_size == 0)
        {
          chunks_by64k = total_number_of_bytes / basic_strip_size;
        }
        else
        {
          chunks_by64k = total_number_of_bytes / basic_strip_size + 1;
        }

        if (chunks_by64k % pool_size == 0)
        {
          chunks_by64k_pp = chunks_by64k / pool_size;
        }
        else
        {
          chunks_by64k_pp = chunks_by64k / pool_size + 1;
        }
        chunk_size = chunks_by64k_pp * basic_strip_size;
      }

      //More options here for optimizations
      //http://www2.cs.uh.edu/~gabriel/courses/cosc6374_s08/ParCo_18_ParallelIO3.pdf
      //http://mpi.deino.net/mpi_functions/MPI_File_write_ordered.html
      if ((my_rank == 0) && (set_striping_size_flag == 1))
      {
        //Set the striping size = chunk_size and also create a new file
        set_striping_size(filename_output, chunk_size);
#ifdef DEBUG
        if (ite % 10 == 0)
          printf("Set striping size to %llu, for file [%s] \n", chunk_size, filename_output);
#endif
      }

      //Wait here for the finish of set_striping_size
      MPI_Barrier(MPI_COMM_WORLD);

      //Set some MPI-IO hints
      MPI_Info infoo;
      char stripe_factor[256], stripe_unit[256]; //cb_buffer_size[256] = "400000000";
      MPI_Info_create(&infoo);
      MPI_Info_set(infoo, "access_style", "write_once, sequential");
      sprintf(stripe_factor, "%d ", OST_NUMS);
      sprintf(stripe_unit, "%zu ", chunk_size);
      MPI_Info_set(infoo, "striping_factor", stripe_factor);
      MPI_Info_set(infoo, "striping_unit", stripe_unit);
      MPI_Info_set(infoo, "cb_nodes", stripe_factor);

      //Open the file on Disk
      file_open_error = MPI_File_open(MPI_COMM_WORLD, filename_output, MPI_MODE_CREATE | MPI_MODE_WRONLY, infoo, &fho);
      //MPI_File_set_atomicity(fho, 0);
      if (file_open_error != MPI_SUCCESS)
      {
        char error_string[BUFSIZ];
        int length_of_error_string, error_class;
        MPI_Error_class(file_open_error, &error_class);
        MPI_Error_string(error_class, error_string, &length_of_error_string);
        printf("%3d: %s\n", my_rank, error_string);
        MPI_Error_string(file_open_error, error_string, &length_of_error_string);
        printf("%3d: %s\n", my_rank, error_string);
        MPI_Abort(MPI_COMM_WORLD, file_open_error);
      }
      //MPI_File_set_size(fho, total_number_of_bytes);
      //MPI_File_preallocate(fho, total_number_of_bytes);

      //#ifdef DEBUG
      if (my_rank == 0)
      {
        printf("Start to move file[%s] from BB to disk, chunk size = %zu  \n", filename_output, chunk_size);
      }
      //#endif

      if (result_file_flag == 1 && my_rank == 0)
        fprintf(rfp, "Start to move file[%s] from BB to disk, chunk size = %zu  \n", filename_output, chunk_size);

      //Start to read and write data
      MPI_Offset iteration, total_chunks;
      fflush(stdout);
      if (total_number_of_bytes % chunk_size == 0)
      {
        total_chunks = total_number_of_bytes / chunk_size;
      }
      else
      {
        total_chunks = total_number_of_bytes / chunk_size + 1;
      }
      if (total_chunks % pool_size == 0)
      {
        iteration = total_chunks / pool_size;
      }
      else
      {
        iteration = total_chunks / pool_size + 1;
      }

      read_buffer = (char *)malloc(chunk_size);
      if (read_buffer == NULL)
      {
        printf("Chunk size is too large to overflow the memory  !\n ");
        exit(-1);
      }

#ifdef DEBUG
      if ((my_rank == 0) && (ite % 10 == 0))
        printf("Total chunks = %llu, iterations = %llu, chunk_size = %u \n ", total_chunks, iteration, chunk_size);
#endif

      MPI_Offset ii = 0, chunk_offset, my_current_id = my_rank;
      int io_size, previous_write_io_size, count, ret, last_write_req_issued = 0;
      MPI_Request write_req, read_req;

      //Restart from the last chunks, which failed for some reason
      if (ops_journal_flag == 1)
      {
        if (last_moved_chunk_batch > 0)
        {
          ii = last_moved_chunk_batch;
          my_current_id = pool_size * ii + my_rank;
        }
        else
        {
          ii = 0;
        }
      }

      for (; ii < iteration; ii++)
      {
        if (my_current_id >= total_chunks)
        {
          if (collective_io == 0)
            break;
          io_size = 0;                          //or break
          chunk_offset = total_number_of_bytes; //To the end
        }
        else
        {
          chunk_offset = my_current_id * chunk_size;
          if (chunk_offset > total_number_of_bytes)
          {
            //io_size =  0; //or break
            if (collective_io == 0)
              break;
          }

          if (chunk_offset + chunk_size > total_number_of_bytes)
          {
            io_size = total_number_of_bytes - chunk_offset;
          }
          else
          {
            io_size = chunk_size;
          }
        }

#ifdef DEBUG
        if (my_rank == 0 || my_rank == (pool_size - 1) || my_rank == (pool_size - 2))
          printf("rank = %d, chunk_offset = %llu, ii = %llu, io_size = %llu \n ", my_rank, chunk_offset, ii, io_size);
#endif

        //Read data
        start = MPI_Wtime();
        if (async_io == 1)
        {
          ret = MPI_File_iread_at(fh, chunk_offset, read_buffer, io_size, MPI_BYTE, &read_req);
        }
        else
        {
          //MPI_File_seek(fh,  chunk_offset, MPI_SEEK_SET);
          ret = MPI_File_read_at(fh, chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
        }
        if (ret != MPI_SUCCESS)
        {
          printf("Something is worong in reading ! \n");
          exit(-1);
        }
        if (async_io == 1)
        {
          MPI_Wait(&read_req, &status);
        }

        MPI_Get_count(&status, MPI_BYTE, &count);
        if (count != io_size)
        {
          printf("In reading, size mismatch : count = %d, io_size = %d ! \n", count, io_size);
          exit(-1);
        }

        finish = MPI_Wtime();
        read_time = read_time + (finish - start);

        //MPI_Barrier(MPI_COMM_WORLD);

        //Write data
        start = MPI_Wtime();
        rank0_write_time_start = MPI_Wtime();
        if (collective_io == 0)
        {
          if (async_io == 1)
          {
            if (first_write == 1)
            {
              first_write = 0;
            }
            else
            {
              MPI_Wait(&write_req, &status);
              MPI_Get_count(&status, MPI_BYTE, &count);
              if (count != previous_write_io_size)
              {
                printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, previous_write_io_size);
                //exit(-1);
              }
            }
            ret = MPI_File_iwrite_at(fho, chunk_offset, read_buffer, io_size, MPI_BYTE, &write_req);
            previous_write_io_size = io_size;
            last_write_req_issued = 1;
          }
          else
          {
            //MPI_File_seek(fho, chunk_offset, MPI_SEEK_SET);
            ret = MPI_File_write_at(fho, chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
          }
        }
        else
        {
          //ret = MPI_File_write_ordered(fho, read_buffer, io_size, MPI_BYTE, &status);
          ret = MPI_File_write_at_all(fho, chunk_offset, read_buffer, io_size, MPI_BYTE, &status);
        }
        if (ret != MPI_SUCCESS)
        {
          printf("Something is worong in wrting ! \n");
          exit(-1);
        }

        if (async_io == 0)
        {
          MPI_Get_count(&status, MPI_BYTE, &count);
          if (count != io_size)
          {
            printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, io_size);
            exit(-1);
          }
        }

        finish = MPI_Wtime();
        rank0_write_time_end = MPI_Wtime();
        write_time = write_time + (finish - start);
        rank0_write_time = rank0_write_time + (rank0_write_time_end - rank0_write_time_start);
        my_current_id = my_current_id + pool_size;

        if (ops_journal_flag == 1)
        {
          MPI_Barrier(MPI_COMM_WORLD);
          if (my_rank == 0) //move log forward
            update_log_file(filename);
        }
      } //End of sinlge file

      //wait until the last write to finish
      if ((async_io == 1) && (last_write_req_issued == 1))
      {
        start = MPI_Wtime();
        MPI_Wait(&write_req, &status);
        MPI_Get_count(&status, MPI_BYTE, &count);
        if (count != previous_write_io_size)
        {
          printf("In writing, size mismatch: count = %d, io_size = %d ! \n", count, previous_write_io_size);
        }
        finish = MPI_Wtime();
        rank0_write_time_end = MPI_Wtime();
        write_time = write_time + (finish - start);
        rank0_write_time = rank0_write_time + (rank0_write_time_end - rank0_write_time_start);
      }
#ifdef DEBUG
      if (my_rank == 0 && ite == 0)
        printf("Writing is done ! \n ");
#endif

      if (my_rank == 0 && last_moved_chunk_batch > 0 && ops_journal_flag == 1)
      {
        MPI_Allreduce(&restart_time, &max_restart_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        MPI_Allreduce(&restart_time, &min_restart_time, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
        printf("Restart time (max)   = %f seconds, min = %f \n", max_restart_time, min_restart_time);
      }

      fflush(stdout);

      MPI_File_close(&fh);
      MPI_File_close(&fho);

      MPI_Barrier(MPI_COMM_WORLD);
      if (ops_journal_flag == 1 && my_rank == 0)
      {
        delete_log_file(filename);
      }

      free(read_buffer);
    stage_out:
      start = MPI_Wtime();
      if (my_rank == 0)
      {
        //printf("I am here ! Test \n \n");

        char sv[256];
        sprintf(sv, "%d", DE_WRITE_DISK_DONE);
        UpdateRecordVOL(filename, DE_TYPE_S, sv);
        if (remove_file_flag == 1)
          MPI_File_delete(filename, MPI_INFO_NULL);
#ifdef DEBUG
        if (ite % 100 == 0)
          printf("  Done and Deleted, Waiting for next file .... \n");
#endif
      }
      finish = MPI_Wtime();
      metadata_time = metadata_time + (finish - start);

      ite++;
      if (ite > MAX_ITERATION)
        break;
    }

    MPI_Allreduce(&read_time, &longest_io_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    read_time_all = longest_io_time;

    MPI_Allreduce(&read_time, &longest_io_time, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    min_read_time = longest_io_time;

    MPI_Allreduce(&write_time, &longest_io_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    write_time_all = longest_io_time;

    MPI_Allreduce(&write_time, &longest_io_time, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    min_write_time = min_write_time + longest_io_time;

    if (my_rank % 100 == 0)
      printf("Write time      on rank %d, sum = %f, ave = %f s \n", my_rank, write_time, write_time / ite);

    //Todo: we need method to print corret information when fake_error_flag==1
    if (my_rank == 0 && fake_error_flag == 0)
    {

      printf("=====================================\n");
      printf("      Summary Information \n");
      printf("=====================================\n");

      printf("Total files        = %d \n", ite);
      printf("Total read time (max)    = %f seconds, ave = %f \n", read_time_all, read_time_all / ite);
      printf("Total write time (max)   = %f seconds, ave = %f \n", write_time_all, write_time_all / ite);
      printf("Total read time (min)    = %f seconds, ave = %f \n", min_read_time, min_read_time / ite);
      printf("Total write time (min)   = %f seconds, ave = %f \n", min_write_time, min_write_time / ite);
      printf("Total size         = %f GB\n", data_size_all);
      printf("Average read  rate = %f GB/s\n", data_size_all / read_time_all);
      printf("Average write rate = %f GB/s\n", data_size_all / write_time_all);
      printf("Update metadata on rank 0, sum = %f, ave = %f s \n", metadata_time, metadata_time / ite);
      printf("Write time      on rank 0, sum = %f, ave = %f s \n", rank0_write_time, rank0_write_time / ite);
      printf("=====================================\n");

      if (result_file_flag == 1)
      {
        fprintf(rfp, "=====================================\n");
        fprintf(rfp, "      Summary Information \n");
        fprintf(rfp, "=====================================\n");

        fprintf(rfp, "Total files        = %d \n", ite);
        fprintf(rfp, "Total read time (max)    = %f seconds, ave = %f \n", read_time_all, read_time_all / ite);
        fprintf(rfp, "Total write time (max)   = %f seconds, ave = %f \n", write_time_all, write_time_all / ite);
        fprintf(rfp, "Total read time (min)    = %f seconds, ave = %f \n", min_read_time, min_read_time / ite);
        fprintf(rfp, "Total write time (min)   = %f seconds, ave = %f \n", min_write_time, min_write_time / ite);
        fprintf(rfp, "Total size         = %f GB\n", data_size_all);
        fprintf(rfp, "Average read  rate = %f GB/s\n", data_size_all / read_time_all);
        fprintf(rfp, "Average write rate = %f GB/s\n", data_size_all / write_time_all);
        fprintf(rfp, "Update metadata on rank 0, sum = %f, ave = %f s \n", metadata_time, metadata_time / ite);
        fprintf(rfp, "Write time      on rank 0, sum = %f, ave = %f s \n", rank0_write_time, rank0_write_time / ite);
        fprintf(rfp, "=====================================\n");
        fclose(rfp);
      }
    }
  }
  else
  { // (prefetch_flag == 1)
    //
    // Pre-fetech the data
    //
    int app_read_process_count;
    unsigned long long *next_blocks_list;
    int broadcast_flag = 0;
    int next_blocks_list_size = pool_size;
    next_blocks_list = malloc(sizeof(unsigned long long) * next_blocks_list_size);
    int data_rank;
    unsigned long long chunk_size_guess[DE_MAX_DIMS];
    unsigned long long dims_size[DE_MAX_DIMS];
    int stop_prefetch_out_of_boundary_flag = 0;
    int chunk_size_temp[DE_MAX_DIMS];
    convert_str_vector(array_rank, ghost_size_str, ghost_size_vector);
    convert_str_vector(array_rank, tile_size_str, chunk_size_temp);
    int ti;
    for (ti = 0; ti < array_rank; ti++)
      tile_size_vector[ti] = chunk_size_temp[ti];

    double prediction_time = 0, prefetch_time = 0, prefetch_time_min, prefetch_time_max, prefetch_time_ave;
    int predict_counts = 0, prefetch_counts = 0;

    if (my_rank == 0)
    {
      printf("Array rank: %d \n ", array_rank);
      printf("Tile size: ");
      int ij;
      for (ij = 0; ij < array_rank; ij++)
      {
        printf(", %llu (guess = %llu)", tile_size_vector[ij], chunk_size_guess[ij]);
      }
      printf("\n Ghost size: ");
      for (ij = 0; ij < array_rank; ij++)
      {
        printf(", %d", ghost_size_vector[ij]);
      }
      printf("\n");
    }

    fflush(stdout);

    while (1)
    {
      //Wait the next available file
      char bb_cache_dir[NAME_LENGTH], dk_fn[NAME_LENGTH], dk_group[NAME_LENGTH], dk_dataset[NAME_LENGTH], bb_fn[NAME_LENGTH];
      if (my_rank == 0)
      {
        //printf("Going to find the prefetch file \n");
        //fflush(stdout);
        //dims_size is not used, it is filled by get_disk_data_type
        rf = next_prefetch_file(bb_cache_dir, dk_fn, dk_group, dk_dataset, bb_fn, array_rank, tile_size_vector, dims_size, &app_read_process_count);
        data_rank = array_rank;
        if (rf == -1)
          exit(-1);

        start = MPI_Wtime();
        //perform prediction
        if (strcmp(dk_fn, "NULL") != 0 && strcmp(bb_fn, "NULL") != 0)
        {
          if (next_blocks_list_size != app_read_process_count)
          {
            next_blocks_list_size = app_read_process_count;
            free(next_blocks_list);
            next_blocks_list = malloc(sizeof(unsigned long long) * next_blocks_list_size);
          }
          get_disk_data_dims_size(dk_fn, dk_group, dk_dataset, dims_size);
          if (predict_next_blocks(bb_cache_dir, next_blocks_list, next_blocks_list_size, array_rank, dims_size, tile_size_vector) == -1)
          {
            //No information to predit: no data to prefetch
            continue;
          }
        }
        finish = MPI_Wtime();
        predict_counts++;
        prediction_time = prediction_time + (finish - start);
      }
      fflush(stdout);

      MPI_Barrier(MPI_COMM_WORLD);

      MPI_Bcast(bb_fn, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(dk_fn, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      //Finish signal of the simulaiton job
      if (strcmp(dk_fn, "NULL") == 0 && strcmp(bb_fn, "NULL") == 0)
      {
        break;
      }

      MPI_Bcast(dk_group, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(dk_dataset, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(bb_cache_dir, NAME_LENGTH, MPI_CHAR, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(&app_read_process_count, 1, MPI_INT, MASTER_RANK, MPI_COMM_WORLD);
      if (next_blocks_list_size != app_read_process_count)
      {
        next_blocks_list_size = app_read_process_count;
        free(next_blocks_list);
        next_blocks_list = malloc(sizeof(unsigned long long) * next_blocks_list_size);
      }
      MPI_Bcast(next_blocks_list, next_blocks_list_size, MPI_UNSIGNED_LONG_LONG, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(tile_size_vector, array_rank, MPI_UNSIGNED_LONG_LONG, MASTER_RANK, MPI_COMM_WORLD);
      MPI_Bcast(dims_size, array_rank, MPI_UNSIGNED_LONG_LONG, MASTER_RANK, MPI_COMM_WORLD);

      int bi;
      if (my_rank == 0)
      { //|| my_rank == (pool_size - 1)
        printf("my rank = %d, bb_cache_dir = %s, dk_fn = %s, dk_group = %s, dk_dataset = %s, chunk_size = (%llu, %llu), dim_size = (%llu, %llu), next_blocks_list_size = %d, predicted blocks:  ", my_rank, bb_cache_dir, dk_fn, dk_group, dk_dataset, tile_size_vector[0], tile_size_vector[1], dims_size[0], dims_size[1], next_blocks_list_size);
        if (next_blocks_list_size > 10)
        {
          for (bi = 0; bi < 5; bi++)
            printf(", %llu ", next_blocks_list[bi]);
          printf(" ... ");
          for (bi = next_blocks_list_size - 5; bi < next_blocks_list_size; bi++)
            printf(", %llu ", next_blocks_list[bi]);
        }
        else
        {
          for (bi = 0; bi < next_blocks_list_size; bi++)
            printf(", %llu ", next_blocks_list[bi]);
        }
        printf("\n");
      }

      fflush(stdout);

      int next_blocks_list_index = my_rank;
      start = MPI_Wtime();
      if (collective_io == 1 && (next_blocks_list_size % pool_size == 0))
      {
        collective_io = 1;
      }
      else
      {
        collective_io = 0;
      }
      while (next_blocks_list_index < next_blocks_list_size)
      {
        stop_prefetch_out_of_boundary_flag = prefetch_block(dk_fn, dk_group, dk_dataset, next_blocks_list[next_blocks_list_index], array_rank, chunk_size_temp, ghost_size_vector, bb_cache_dir, collective_io);
        next_blocks_list_index = next_blocks_list_index + pool_size;
      }
      finish = MPI_Wtime();
      prefetch_counts++;
      prefetch_time = prediction_time + (finish - start);

      if (stop_prefetch_out_of_boundary_flag == -1)
        break;
    }
    free(next_blocks_list);

    MPI_Allreduce(&prefetch_time, &prefetch_time_max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&prefetch_time, &prefetch_time_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);

    if (my_rank == 0)
    {
      printf("=====================================\n");
      printf("      Summary Information for Prefetch \n");
      printf("=====================================\n");
      printf("Total prefetch batch      =  %d \n", prefetch_counts);
      printf("Total prefetch time (max)    = %f seconds, ave = %f \n", prefetch_time_max, prefetch_time_max / prefetch_counts);
      printf("Total prefetch time (min)    = %f seconds, ave = %f \n", prefetch_time_min, prefetch_time_min / prefetch_counts);
      printf("Prediction time, sum = %f, predict_counts = %d, ave = %f s \n", prediction_time, predict_counts, prediction_time / predict_counts);
      printf("=====================================\n");

      if (result_file_flag == 1)
      {
        fprintf(rfp, "=====================================\n");
        fprintf(rfp, "      Summary Information \n");
        fprintf(rfp, "=====================================\n");
        fprintf(rfp, "Total prefetch batch      =  %d \n", prefetch_counts);
        fprintf(rfp, "Total prefetch time (max)    = %f seconds, ave = %f \n", prefetch_time_max, prefetch_time_max / prefetch_counts);
        fprintf(rfp, "Total prefetch time (min)    = %f seconds, ave = %f \n", prefetch_time_min, prefetch_time_min / prefetch_counts);
        fprintf(rfp, "Prediction time, sum = %f, predict_counts = %d, ave = %f s \n", prediction_time, predict_counts, prediction_time / predict_counts);
        fprintf(rfp, "=====================================\n");
        fclose(rfp);
      }
    }
  }
stage_in_exit:

  if (my_rank == 0)
    printf("DataElevator stops work ! \n");

  MPI_Finalize();
  fflush(stdout);
  exit(0);
}

int write_block_to_bb(char *bb_cache_dir, void *buf, size_t buf_type_size, int buf_num, unsigned long long start, unsigned long long end)
{
  //char f_name_str[NAME_LENGTH] = {0};
  //sprintf(f_name_str, "%s/%d", bb_cache_dir, file_name_int);
  //printf("\n\nWrite pre-feteched data to %s \n\n", f_name_str);

  char *chunk_file_path_name = malloc(NAME_LENGTH);
  merge_chunk_file_name_start_end(bb_cache_dir, start, end, chunk_file_path_name);
  //printf("bb_cache_dir: %s \n", bb_cache_dir);
  //FILE *fp;
  //fp=fopen(chunk_file_path_name, "wb");
  //fwrite(buf, buf_type_size, buf_num , fp);
  //fflush(fp);
  //fclose(fp);
  int fid;
  //fid = open(chunk_file_path_name, O_WRONLY|O_CREAT);
  fid = open(chunk_file_path_name, O_WRONLY | O_CREAT, S_IRWXU);
  write(fid, buf, buf_type_size * buf_num);
  close(fid);
  free(chunk_file_path_name);
  return 0;
}

int get_disk_data_dims_size(char *dk_fn, char *dk_group, char *dk_dataset, unsigned long long *dims_size)
{
  //hid_t plist_id2      = H5Pcreate(H5P_FILE_ACCESS);
  //H5Pset_fapl_mpio(plist_id2, MPI_COMM_SELF, MPI_INFO_NULL);

  //printf("dk_fn  = %s in get type \n", dk_fn); fflush(stdout);

  hid_t fid = H5Fopen(dk_fn, H5F_ACC_RDONLY, H5P_DEFAULT);
  //hid_t fid  = H5Fopen(dk_fn, H5F_ACC_RDWR, plist_id2);
  if (fid < 0)
  {
    printf("Error happens in open file %s \n ", dk_fn);
    exit(-1);
  }
  //printf("open file dk_fn  = %s in get type, done \n", dk_fn); fflush(stdout);

  hid_t gid = -1, did;

  if (strcmp(dk_group, "/") != 0)
  {
    //std::cout << "Open Group : " << gn << std::endl;
    gid = H5Gopen(fid, dk_group, H5P_DEFAULT);
    did = H5Dopen(gid, dk_dataset, H5P_DEFAULT);
  }
  else
  {
    did = H5Dopen(fid, dk_dataset, H5P_DEFAULT);
  }
  hid_t datatype = H5Dget_type(did); /* datatype handle */
  int ret_size;
  switch (H5Tget_class(datatype))
  {
  case H5T_INTEGER:
    ret_size = sizeof(int);
    //printf("Data type is [int]. \n");
    break;
  case H5T_FLOAT:
    ret_size = sizeof(float);
    //printf("Data type is [float] . \n");
    break;
  default:
    printf("Unsupported datatype in pre-fetch in Data Elevator! \n");
    exit(-1);
    break;
  }

  hid_t dataspace_id = H5Dget_space(did);
  int data_rank = H5Sget_simple_extent_ndims(dataspace_id);
  hsize_t *dims_out = malloc(sizeof(hsize_t) * data_rank);
  H5Sget_simple_extent_dims(dataspace_id, dims_out, NULL);
  int j;
  for (j = 0; j < data_rank; j++)
  {
    dims_size[j] = dims_out[j];
  }

  //H5Pclose(plist_id2);
  H5Sclose(dataspace_id);

  H5Dclose(did);
  if (gid != -1)
    H5Gclose(gid);
  H5Fclose(fid);

  return ret_size;
}

int row_major_order_reverse(unsigned long long offset, int rank, unsigned long long *dims, unsigned long long *coord)
{
  int i;
  for (i = rank - 1; i >= 1; i--)
  {
    coord[i] = offset % dims[i];
    offset = offset / dims[i];
  }
  coord[0] = offset;

  return 0;
}

//return value:
// -1: out-of-boundary
//  0: within the boundary
int figure_out_offset_count(const int data_rank, unsigned long long *dims_out, int *chunk_size, int *ghost_size, unsigned long long block_id, unsigned long long *offset, unsigned long long *count, unsigned long long *start_offset, unsigned long long *end_offset)
{
  //printf("data_rank = %d, my block id = %llu \n", data_rank, block_id);

  //printf("dims_out= (%llu, %llu), chunk_size = (%d, %d), ghost_size=  (%d, %d) \n", dims_out[0],  dims_out[1], chunk_size[0], chunk_size[1], ghost_size[0], ghost_size[1]);

  unsigned long long chunked_dims[data_rank], chunked_coord[data_rank], end[data_rank];
  int i;
  unsigned long long total_chunks = 1;

  for (i = 0; i < data_rank; i++)
  {
    chunked_dims[i] = dims_out[i] / chunk_size[i];
    //printf("%llu \n", chunked_dims[i]);
    total_chunks = total_chunks * chunked_dims[i];
  }
  if (block_id >= total_chunks)
    return -1;

  row_major_order_reverse(block_id, data_rank, chunked_dims, chunked_coord);

  for (i = 0; i < data_rank; i++)
  {
    offset[i] = chunked_coord[i] * chunk_size[i];
    count[i] = chunk_size[i];

    if (offset[i] > ghost_size[i])
    {
      offset[i] = offset[i] - ghost_size[i];
      count[i] = count[i] + ghost_size[i];
    }

    if (offset[i] + count[i] + ghost_size[i] <= dims_out[i])
    {
      count[i] = count[i] + ghost_size[i];
    }
    end[i] = offset[i] + count[i] - 1;
  }

  ROW_MAJOR_ORDER_MACRO(dims_out, data_rank, offset, *start_offset);
  ROW_MAJOR_ORDER_MACRO(dims_out, data_rank, end, *end_offset);

  //#ifdef DEBUG
  //printf("dims_out= (%llu, %llu), chunk_size = (%d, %d), chunkd_dims =  (%llu, %llu), chunked_coord=(%llu, %llu), total_chunks = %llu, offset = (%llu, %llu), count = (%llu, %llu), ghost_size=  (%d, %d) \n", dims_out[0],  dims_out[1], chunk_size[0], chunk_size[1], chunked_dims[0], chunked_dims[1],  chunked_coord[0],  chunked_coord[1], total_chunks, offset[0], offset[1], count[0], count[1], ghost_size[0], ghost_size[1]);
  //#endif
  return 0;
}

//return value
// -1: out-of-boundary or other errors
// 0:  read with data.
int prefetch_block(char *dk_fn, char *dk_group, char *dk_dataset, unsigned long long block_id, int data_rank, int *chunk_size, int *ghost_size, char *bb_cache_dir, int collective_io_flag)
{
  hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

  //printf("prefetch_block : dk_fn = %s \n", dk_fn); fflush(stdout);

  hid_t fid = H5Fopen(dk_fn, H5F_ACC_RDONLY, plist_id);
  //hid_t fid  = H5Fopen(dk_fn, H5F_ACC_RDWR, plist_id);
  if (fid < 0)
  {
    printf("Error happens in open file %s \n", dk_fn);
    exit(-1);
  }

  hid_t gid = -1, did;

  if (strcmp(dk_group, "/") != 0)
  {
    //std::cout << "Open Group : " << gn << std::endl;
    gid = H5Gopen(fid, dk_group, H5P_DEFAULT);
    did = H5Dopen(gid, dk_dataset, H5P_DEFAULT);
  }
  else
  {
    did = H5Dopen(fid, dk_dataset, H5P_DEFAULT);
  }
  hid_t datatype = H5Dget_type(did); /* datatype handle */
  hid_t plist_cio_id = H5Pcreate(H5P_DATASET_XFER);
  //if(collective_io_flag == 1)
  H5Pset_dxpl_mpio(plist_cio_id, H5FD_MPIO_COLLECTIVE);

  hid_t dataspace_id = H5Dget_space(did);
  //update the data_rank on rank 1, 2, 3 ...
  data_rank = H5Sget_simple_extent_ndims(dataspace_id);

  hsize_t *dims_out = malloc(sizeof(hsize_t) * data_rank);
  H5Sget_simple_extent_dims(dataspace_id, dims_out, NULL);

  hsize_t *offset, *count;
  offset = malloc(sizeof(hsize_t) * data_rank);
  count = malloc(sizeof(hsize_t) * data_rank);
  void *buf;
  size_t buf_size, data_type_size;
  unsigned long long start_offset, end_offset;

  int out_of_boundary_flag = figure_out_offset_count(data_rank, dims_out, chunk_size, ghost_size, block_id, offset, count, &start_offset, &end_offset);

  //return 0;
  if (out_of_boundary_flag == -1)
  {
    //printf("Out-of-boundary ! \n");
    free(dims_out);
    free(offset);
    free(count);
    return out_of_boundary_flag;
  }

  //else{
  //  printf("In-of-boundary ! \n");
  //}
  buf_size = 1;
  int i;
  for (i = 0; i < data_rank; i++)
  {
    buf_size = buf_size * count[i];
  }

  int ret;
  hid_t memspace_id = H5Screate_simple(data_rank, count, NULL);
  ;
  H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);
  switch (H5Tget_class(datatype))
  {
  case H5T_INTEGER:
    buf = malloc(sizeof(int) * buf_size);
    data_type_size = sizeof(int);
    ret = H5Dread(did, H5T_NATIVE_INT, memspace_id, dataspace_id, plist_cio_id, buf);
    break;
  case H5T_FLOAT:
    buf = malloc(sizeof(float) * buf_size);
    data_type_size = sizeof(float);
    ret = H5Dread(did, H5T_NATIVE_FLOAT, memspace_id, dataspace_id, plist_cio_id, buf);
    break;
  default:
    printf("Unsupported datatype in pre-fetch in Data Elevator! \n");
    exit(-1);
    break;
  }

  write_block_to_bb(bb_cache_dir, buf, data_type_size, buf_size, start_offset, end_offset);
  free(buf);
  free(dims_out);
  free(offset);
  free(count);
  //H5Pclose(plist_id);
  H5Sclose(dataspace_id);
  H5Dclose(did);
  if (gid != -1)
    H5Gclose(gid);
  H5Fclose(fid);

  return 0;
}

int compULL(const void *elem1, const void *elem2)
{
  int f = *((unsigned long long *)elem1);
  int s = *((unsigned long long *)elem2);
  if (f > s)
    return 1;
  if (f < s)
    return -1;
  return 0;
}

// retun
// -1:  no information to predict for prefetch
// 1:  has information to predict for for prefetch
int predict_next_blocks(char *bb_cache_dir, unsigned long long *next_blocks_list, int next_blocks_size, int data_rank, unsigned long long *dims_size, unsigned long long *chunk_size)
{
  //Read the file list under bb_cache_dir
  tinydir_dir dir;
  int i;
  unsigned long long *chunk_file_id, *chunk_start_offset, *chunk_end_offset, max_chunk_id;
  tinydir_open_sorted(&dir, bb_cache_dir);
  chunk_start_offset = malloc(dir.n_files * sizeof(unsigned long long));
  chunk_end_offset = malloc(dir.n_files * sizeof(unsigned long long));
  chunk_file_id = malloc(dir.n_files * sizeof(unsigned long long));
  max_chunk_id = 1;

  int jj;
  for (jj = 0; jj < data_rank; jj++)
    max_chunk_id = dims_size[jj] / chunk_size[jj] * max_chunk_id;

  int chunk_file_id_size = 0;
  //printf("cached chunk file names (dims_size = %llu, %llu):", dims_size[0], dims_size[1]);
  for (i = 0; i < dir.n_files; i++)
  {
    tinydir_file file;
    tinydir_readfile_n(&dir, &file, i);
    if ((!file.is_dir) && strcmp(file.name, ".") && strcmp(file.name, ".."))
    {
      if (partial_prefetch_flag == 1 && file._s.st_size == 0)
      {
        printf("Skip zero sized file %s \n", file.name);
        continue;
      }
      //printf("%s ( ", file.name);
      char *token = strtok(file.name, "-");
      chunk_start_offset[chunk_file_id_size] = strtoull(token, NULL, 10);
      token = strtok(NULL, "-");
      chunk_end_offset[chunk_file_id_size] = strtoull(token, NULL, 10);
      //printf("so=%llu, eo=%llu, ", chunk_start_offset[chunk_file_id_size],chunk_end_offset[chunk_file_id_size]);
      chunk_file_id[chunk_file_id_size] = convert_start_end_offset_to_chunk_id(data_rank, chunk_start_offset[chunk_file_id_size], chunk_end_offset[chunk_file_id_size], dims_size, chunk_size);
      //printf("dims_size=(%llu, %llu), chunk_size=(%llu, %llu), cid=%llu), ", dims_size[0], dims_size[1], chunk_size[0], chunk_size[1], chunk_file_id[chunk_file_id_size]);
      if (chunk_file_id[chunk_file_id_size] < max_chunk_id)
        chunk_file_id_size = chunk_file_id_size + 1;
    }
  }
  //printf("\n");  fflush(stdout);
  tinydir_close(&dir);
  //Find next_blocks_size
  //Todo: right now, we assume that next_blocks_size = the number of client chunks

  if (chunk_file_id_size == 0)
  {
    //free(chunk_file_id); free(chunk_end_offset); free(chunk_start_offset);return -1;
    //No return here
    int j;
    next_blocks_list[0] = 0;
    for (j = 1; j < next_blocks_size; j++)
    {
      next_blocks_list[j] = next_blocks_list[j - 1] + 1;
    }
  }
  else
  {
    //Partial information available
    if (chunk_file_id_size < next_blocks_size)
      return -1;
    //Have enough to predict
    qsort(chunk_file_id, chunk_file_id_size, sizeof(unsigned long long), compULL);
    printf("Chunk id: ");
    for (i = 0; i < 5; i++)
      printf("%llu,", chunk_file_id[i]);
    printf(".....");
    for (i = chunk_file_id_size - 5; i < chunk_file_id_size; i++)
      printf("%llu,", chunk_file_id[i]);
    printf("\n");
    fflush(stdout);
    int j;
    unsigned long long delta;
    //Predict next blocks
    if (chunk_file_id_size >= 2)
    {
      delta = chunk_file_id[chunk_file_id_size - 1] - chunk_file_id[chunk_file_id_size - 2];
    }
    else
    {
      delta = 1;
    }
    next_blocks_list[0] = chunk_file_id[chunk_file_id_size - 1] + delta;
    for (j = 1; j < next_blocks_size; j++)
    {
      next_blocks_list[j] = next_blocks_list[j - 1] + delta;
      //if(next_blocks_list[j] >= max_chunk_id) {break;}
    }
  }
  free(chunk_file_id);
  free(chunk_end_offset);
  free(chunk_start_offset);
  return 1;
}

//
// Wait next available file to move
//
int next_file(char *bb_fn, char *dk_fn)
{
  DEMetaRecord d2bmetast;
  int ret;

  while (1)
  {
    ret = NextBBDoneRecord(&d2bmetast);
    if (ret == 0)
    {
      if ((strcmp(d2bmetast.dkname, "NULL") == 0) && (strcmp(d2bmetast.bbname, "NULL") == 0))
      {
        strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
        strncpy(bb_fn, d2bmetast.bbname, sizeof(d2bmetast.bbname));
        return 2; //End of simulation
      }

      strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
      strncpy(bb_fn, d2bmetast.bbname, sizeof(d2bmetast.bbname));
      return 0;
    }

    if (ret == -1)
      return -1;

    //Since there is no new file coming
    //do check app_failed_flag to see wether we should exit
    if (app_failed_flag == 1)
    {
      strncpy(dk_fn, "NULL", sizeof("NULL"));
      strncpy(bb_fn, "NULL", sizeof("NULL"));
      return 0;
    }
    sleep(MONITOR_INTERVAL);
  }
}

//
// Wait next available file to move
//
int next_prefetch_file(char *bb_cache_dir, char *dk_fn, char *dk_group, char *dk_dataset, char *bb_fn, int rank, unsigned long long *chunk_size, unsigned long long *dims_size, int *app_read_process_count)
{
  DEMetaRecord d2bmetast;
  int ret;
  //printf("bb_cache_dir = %s \n", "in next_prefetch_file");
  while (1)
  {
    ret = NextBBPrefetchRecord(&d2bmetast, chunk_size, rank);
    if (ret == 0)
    {
      if ((strcmp(d2bmetast.dkname, "NULL") == 0) && (strcmp(d2bmetast.bbname, "NULL") == 0))
      {
        strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
        strncpy(bb_fn, d2bmetast.bbname, sizeof(d2bmetast.bbname));
        return 2; //End of simulation
      }

      strncpy(dk_fn, d2bmetast.dkname, sizeof(d2bmetast.dkname));
      strncpy(dk_group, d2bmetast.groupname, sizeof(d2bmetast.groupname));
      strncpy(dk_dataset, d2bmetast.dsetname, sizeof(d2bmetast.dsetname));
      strncpy(bb_cache_dir, d2bmetast.readdirbb, sizeof(d2bmetast.readdirbb));
      //*rank = d2bmetast.rank;
      *app_read_process_count = d2bmetast.read_mpi_process;
      int j;
      for (j = 0; j < d2bmetast.rank && j < DE_MAX_DIMS; j++)
      {
        //chunk_size[j] = d2bmetast.chks[j];
        dims_size[j] = d2bmetast.dims[j];
      }
      //printf("next_prefetch_file, rank = %d, chunk size (not used)  =  (%llu, %llu), dims size (not used) = (%llu, %llu) \n", d2bmetast.rank, d2bmetast.chks[0], d2bmetast.chks[1], dims_size[0], dims_size[1]);
      //*next_blocks_list_size = d2bmetast.read_mpi_process;
      return 0;
    }

    if (ret == -1)
      return -1;

    //Since there is no new file coming
    //do check app_failed_flag to see wether we should exit
    if (app_failed_flag == 1)
    {
      strncpy(dk_fn, "NULL", sizeof("NULL"));
      strncpy(bb_fn, "NULL", sizeof("NULL"));
      return 0;
    }
    sleep(MONITOR_INTERVAL);
  }
}

int file_exist_b2d(char *filename)
{
  struct stat buffer;
  return (stat(filename, &buffer) == 0);
}

void set_striping_size(char *fn, MPI_Offset ss)
{
  char command[NAME_LENGTH] = {0};
  int sc = OST_NUMS;

  //MPI_File_delete(fn, MPI_INFO_NULL);
  if (file_exist_b2d(fn))
    remove(fn);

  sprintf(command, "lfs setstripe ----stripe-size %llu --count %d  %s \n", ss, sc, fn);
  //system(command);
  FILE *fp;
  fp = popen(command, "r");
  if (fp == NULL)
  {
    printf("Failed to run command [%s], but we still continue to run the code !\n", command);
    //exit(1);
  }
  char path[NAME_LENGTH];
  while (fgets(path, sizeof(path) - 1, fp) != NULL)
  {
    printf("%s", path);
  }
  pclose(fp);

  /*
    sprintf(command, "lfs getstripe %s \n", fn);
    //system(command);
    fp = popen(command, "r");
    if (fp == NULL) {
    printf("Failed to run command [%s]\n", command );
    exit(1);
    }
    sprintf(command, "lfs getstripe %s \n", fn);
    while (fgets(path, sizeof(path)-1, fp) != NULL) {
    printf("%s", path);
    }
    pclose(fp); */
}

#ifndef DE_JOB_H
#define DE_JOB_H
#include <stdio.h>

//Time interval to minoter metadata table
#define MONITOR_INTERVAL 5
//The number of OSTes in Lustre
#define OST_NUMS 248
//The striping size to read/write data
#define BASIC_REQUEST_SIZE 8388608 //8 MB
//The max number of iterations to read/write data
#define MAX_ITERATION 1000000

int next_file(char *bb_fn, char *dk_fn);
void set_striping_size(char *fn, MPI_Offset ss);
int write_block_to_bb(char *bb_cache_dir, void *buf, size_t buf_type_size, int buf_num, unsigned long long start, unsigned long long end);
int get_disk_data_dims_size(char *dk_fn, char *dk_group, char *dk_dataset, unsigned long long *dims_size);
//int get_disk_data_type(char *dk_fn, char *dk_group, char *dk_dataset);
int row_major_order_reverse(unsigned long long offset, int rank, unsigned long long *dims, unsigned long long *coord);
int figure_out_offset_count(const int data_rank, unsigned long long *dims_out, int *chunk_size, int *ghost_size, unsigned long long block_id, unsigned long long *offset, unsigned long long *count, unsigned long long *start_offset, unsigned long long *end_offset);

int prefetch_block(char *dk_fn, char *dk_group, char *dk_dataset, unsigned long long block_id, int data_rank, int *chunk_size, int *ghost_size, char *bb_cache_dir, int collective_io_flag);

int predict_next_blocks(char *bb_cache_dir, unsigned long long *next_blocks_list, int next_blocks_size, int data_rank, unsigned long long *dims_size, unsigned long long *chunk_size);

int next_prefetch_file(char *bb_cache_dir, char *dk_fn, char *dk_group, char *dk_dataset, char *bb_fn, int rank, unsigned long long *chunk_size, unsigned long long *dims_size, int *app_read_process_count);

#endif
